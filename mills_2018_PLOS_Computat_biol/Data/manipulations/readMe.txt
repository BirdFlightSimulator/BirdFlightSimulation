Robin Mills 2018
This is the output of the simulations for "Physics-based simulations of aerial attacks by peregrine falcons reveal that stooping at high speed maximizes catch success against agile prey"
Specifically, this data reproduces the catch success mentioned in section "What mechanisms underlie the increased catch success in a stoop?".

The files are named as follows:
preyManeuver_reactionTimePredator_visualErrorPredator_controlErrorPredator_manipulationNumber

Prey maneuver indices denote the following maneuvers:
1. linear
2. non-smooth
3. smooth

Manipulation numbers indicate:
1. fixed low load factor of 3.8
2. Low level attack, with initial speed of 112ms-1
3. artificial oscillations (set oscillations = 2)
4. fixed low roll acceleration of 1500
5. no manipulation
6. fixed low load factor of 3.8, fixed low roll acceleration of 1500
7. artificial oscillations, fixed low load factor of 3.8, fixed low roll acceleration of 1500
8. artificial oscillations, fixed low load factor of 3.8, fixed low roll acceleration of 1500, initial speed of 112ms-1

Reaction time is indicated in seconds, visual error in radians*1000, and control error is indicated in as a proportion. 
To re-generate the data found on this gitlab repository, run the simulation model under the desired settings, and run this script to generate a GAM-smoothed grid. The grid is used to generate the plots of the paper. 
To generate the plots, see script "step_2.output.R"
