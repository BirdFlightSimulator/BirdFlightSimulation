# This folder contains data and figures of:

Mills, R., Hildenbrandt, H., Taylor, G.K. & Hemelrijk, C.K., (2018). Physics-based simulations of aerial attacks by peregrine falcons reveal 
that stooping at high speed maximizes catch success against agile prey. Manuscript accepted. PLoS computational biology

The organization of the folder is as follows:

* `Data` - Contains output of the simulations in .Rdata file format.
* `Analysis scripts` - Contains the script that process raw simulation output into paper Figures.
* `Code for flight performance` - Contains MATLAB scripts that compute the flight performance of selected model-bird species.
* `Figures` - contains Figures of the PLOS article


The version of the bird flight simulator that is used for this paper is V.1.0 (see commit history).

### Abstract of paper 

The peregrine falcon \textit{Falco peregrinus} is renowned for attacking its prey from high altitude in a fast controlled dive called a stoop. 
Many other raptors employ a similar mode of attack, but the functional benefits of stooping remain obscure. 
Here we investigate whether, when, and why stooping promotes catch success, using a three-dimensional, agent-based modeling approach to simulate attacks of falcons on aerial prey. 
We simulate avian flapping and gliding flight using an analytical quasi-steady model of the aerodynamic forces and moments, parametrized by empirical measurements of flight morphology. 
The model-birds’ flight control inputs are commanded by their guidance system, comprising a phenomenological model of its vision, guidance, and control. 
To intercept its prey, model-falcons use the same guidance law as missiles (pure proportional navigation); this assumption is corroborated by empirical data on peregrine falcons hunting lures. 
We parametrically vary the falcon's starting position relative to its prey, together with the feedback gain of its guidance loop, under differing assumptions regarding its errors and delay in vision and control, and for three different patterns of prey motion. 
We find that, when the prey maneuvers erratically, high-altitude stoops increase catch success compared to low-altitude attacks, but only if the falcon's guidance law is appropriately tuned, and only given a high degree of precision in vision and control. 
Remarkably, the optimal tuning of the guidance law in our simulations coincides closely with what has been observed empirically in peregrines. 
High-altitude stoops are shown to be beneficial because their high airspeed enables production of higher aerodynamic forces for maneuvering, and facilitates higher roll agility as the wings are tucked, each of which is essential to catching maneuvering prey at realistic response delays.

