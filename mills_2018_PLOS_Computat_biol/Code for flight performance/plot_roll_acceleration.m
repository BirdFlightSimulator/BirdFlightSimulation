function [ p ] = plot_roll_acceleration(figureNumber, birds_morph, birds,style)

%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    lWidth = 3;
    mSize = 100;
    labelSize = 1;
    xSpan = 125;
    ySpan = 22;

    if style == 1; fig = figure(figureNumber); else
        fig = figure(1);
        subplot(3,2,figureNumber);
    end
    fig.RendererMode = 'manual';  % use a set with older versions of Matlab
    fig.Renderer = 'painters';
    hold on
    num = 200;
    xlabel('airspeed (ms^{-1})', 'interpreter', 'Tex' )
    ylabel('roll acceleration (ms^{-2})')
    set(gca,'XTick',0:20:400)
    grid on
    xlim([0,120])

    for n = 1:length(birds)
        the_bird = birds{n};
        the_bird_morph = birds_morph{n};
                if (ischar(the_bird_morph.plotColor)); pcolor = strcat('', the_bird_morph.plotColor); else; pcolor = the_bird_morph.plotColor; end
        plot(the_bird.airspeed(the_bird.airspeed < the_bird.top_level_speed),the_bird.max_angular_acc(the_bird.airspeed < the_bird.top_level_speed),  'Color',pcolor, 'LineWidth', lWidth, 'MarkerSize', mSize)
        plot(the_bird.airspeed(the_bird.airspeed < the_bird.top_dive_speed),the_bird.max_angular_acc(the_bird.airspeed < the_bird.top_dive_speed),  'Color',pcolor, 'LineWidth', lWidth, 'lineStyle','--' ,'MarkerSize', mSize)
    end
    limy = ylim;
    ySpan =limy(2) - limy(1) ;
    for n = 1:length(birds) 
        the_bird = birds{n};
        the_bird_morph = birds_morph{n};
        ySizeRectangle = ySpan ./ 35 .* labelSize;
        xSizeRectangle = xSpan ./ 35 .* labelSize;
        if n==4; add = 1000; else; add = 0; end 
        if (ischar(the_bird_morph.plotColor)); pcolor = strcat('', the_bird_morph.plotColor); else; pcolor = the_bird_morph.plotColor; end
        endpoint = find(abs(the_bird.airspeed - the_bird.top_dive_speed) == min(abs(the_bird.airspeed - the_bird.top_dive_speed)))+ add;
        rectangle('Position',[the_bird.airspeed(endpoint - 1500)-xSizeRectangle./2 the_bird.max_angular_acc(endpoint - 1500)-ySizeRectangle./2 xSizeRectangle ySizeRectangle],'Curvature',1, 'FaceColor',[1 1 1],'EdgeColor', pcolor, 'LineWidth',lWidth/2)
        tx = text(the_bird.airspeed(endpoint - 1500),the_bird.max_angular_acc(endpoint - 1500), string(n){1});
        tx.HorizontalAlignment = 'center';
        tx.FontSize = 12;
    end
    title('(d)')
    p = fig;
end

