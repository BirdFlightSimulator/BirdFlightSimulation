%% settings
set(0,'defaultAxesFontSize', 18) 
set(0,'defaultAxesFontName','Times New Roman')
set(0,'defaultTextFontName','Times New Roman')
%set location to save plots
save_folder = 'someFolder\';
%filename of bird morphology
filename_bird_morph = 'bird_properties.xlsm';

%% _______________________load morphology____________________________________
birds_morph = {};
birds_morph{1} = xls_read('Peregrine falcon','male', 'k',filename_bird_morph);
birds_morph{2} = xls_read('Common starling','male', 'g',filename_bird_morph);

%% run the function for calculating the flight performance for each bird
for n = 1:length(birds_morph)
    birds{n} = calculate_flight_performance(birds_morph{n});
end

%% ____________________generate plots and save_______________________________
close all;
type = 1;       %type 1 = individual plots, type 2 = combined plots
p1 = plot_level_acceleration(1, birds_morph,birds,type);
p2 = plot_dive_acceleration(2, birds_morph,birds,type);
p3 = plot_load_factor(3, birds_morph,birds,type);
p4 = plot_roll_acceleration(4,birds_morph,birds,type);
p5 = plot_turn_radius(5,birds_morph,birds,type);


if type == 1
    print(p1,strcat(save_folder,'levelacc'),'-dpdf');
    print(p2,strcat(save_folder,'diveacc'),'-dpdf');
    print(p3,strcat(save_folder,'loadfactor'),'-dpdf');
    print(p4,strcat(save_folder,'rollacc'),'-dpdf');
    print(p5,strcat(save_folder,'turnrad'),'-dpdf');
elseif type == 2
    p6.PaperPosition = [0 0 5000 1000]
    p6.PaperSize = [15 22.5]
    print('-fillpage',strcat(save_folder,'perf_combined'),'-dpdf')
end