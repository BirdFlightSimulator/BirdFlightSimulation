# Bird Flight Simulator, Version 1.0-dev

This gitlab repository contains data and the bird flight simulator first used in:

Mills, R., Hildenbrandt, H., Taylor, G.K. & Hemelrijk, C.K., (2018). Physics-based simulations of aerial attacks by peregrine falcons reveal 
that stooping at high speed maximizes catch success against agile prey. Manuscript accepted. PLoS computational biology

The organization of the repository is as follows:

* `mills_2018_PLOS_Computat_biol` contains data, analysis scripts and code for flight performance graphs of the PLOS manuscript.
* `Bird flight simulator` - Contains the bird flight simulator that lies at the core of the PLOS manuscript.
* `Videos` - Contains example videos of the bird flight simulator.

The bird flight simulator is open source, but we ask you to cite the above paper if you make use of it. The bird flight simulator 1.0 is the first version of this software that has become public.
We encourage others to help us extend our simulator such that many more interesting questions about bird flight may be answered.
Contributors will be listed in our contributions.md. Bird Flight Simulator 1.0 makes use a skeleton architecture for its graphics rendering, developed by Hanno Hildenbrandt called StarDisplay; see the following references:

Hildenbrandt, H., Carere, C., & Hemelrijk, C. K. (2010). Self-organized aerial displays of thousands of starlings: a model. Behavioral Ecology, 21(6), 1349-1359.

Hemelrijk, C. K., van Zuidam, L., & Hildenbrandt, H. (2015). What underlies waves of agitation in starling flocks. Behavioral ecology and sociobiology, 69(5), 755-764.

Hemelrijk, C. K., & Hildenbrandt, H. (2015). Diffusion and topological neighbours in flocks of starlings: relating a model to empirical data. Plos One, 10(5), e0126913.


### Introduction

Peregrine falcons are famed for their high-speed, high-altitude stoops. 
Hunting prey at perhaps the highest speed of any animal places a stooping falcon under extraordinary physical, physiological, and cognitive demands, yet it remains unknown how this behavioural strategy promotes catch success. 
Because the behavioral aspects of stooping are intimately related to its biomechanical constraints, we address this question through an embodied cognition approach. 
The simulator models avian flapping and gliding flight using an analytical quasi-steady model of the aerodynamic forces and moments, parametrized by empirical measurements of flight morphology. 
The model-birds’ flight control inputs are commanded by their guidance system, comprising a phenomenological model of its vision, guidance, and control. 
The PLoS manuscript describes the methods of the simulator in its entire detail.



## Getting started and using the software

### Download

The bird flight simulator can be downloaded by cloning this repository or by downloading the contents as a zip-file.


### Installation and use

1. The bird flight simulator requires Visual Studio 2017 or higher and an NVIDIA graphics card and Windows 7 or higher.
Open BirdFlightSimulator.sln in VS2017 and build in debug or release mode (64 bit). Please contact r.mills@rug.nl for any problems building the model.
To conduct a new experiment, alter the LUA scripts bin\config.lua and bin\experiments.lua.
The main model components are found in StarDisplay\bird.cpp, StarDisplay\predator.cpp, StarDisplay\prey.cpp.

2. The flight performance code under mills_2018_PLOS_Computat_biol requires MATLAB 2014a or higher. Run main.m in MATLAB to generate plots.

3. The script to analyse simulation output require R statistics with the mgcv and ggplot2 packages installed (downloadable via CRAN).

Copyright 2018
