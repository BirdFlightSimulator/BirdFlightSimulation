#include "config.hpp"
#include "libStarDisplay.hpp"

using namespace luabind;


extern void luaopen_libGlm(lua_State* L);
extern void luaopen_libRnd(lua_State* L);
extern void luaopen_libWin(lua_State* L);
extern void luaopen_libParam(lua_State* L);
extern void luaopen_libCamera(lua_State* L);
extern void luaopen_libIText(lua_State* L);
extern "C" LUALIB_API int luaopen_bit(lua_State* L);


namespace libIText
{
  extern class ITextBox* CreateTextBoxCpp(class IText* self, const char* name, const glm::ivec4& box, const class ICamera* camera);
}



// Catch C++ exceptions and convert them to Lua error messages.
// Customize as needed for your own exception classes.
int wrap_cpp_exceptions(lua_State *L, lua_CFunction f)
{
  try {
    return f(L);  // Call wrapped function and return result.
  } catch (const char *s) {  // Catch and convert exceptions.
    lua_pushstring(L, s);
  } catch (std::exception& e) {
    lua_pushstring(L, e.what());
  } catch (...) {
    lua_pushliteral(L, "caught (...)");
  }
  return lua_error(L);  // Rethrow as a Lua error.
}


int pcall_error_callback(lua_State* L)
{
  lua_getfield(L, LUA_GLOBALSINDEX, "debug");
  lua_getfield(L, -1, "traceback");
  lua_pushvalue(L, 1);
  lua_pushinteger(L, 2);
  lua_call(L, 2, 1);
  auto msg = lua_tostring(L, -1);
  lua_getfield(L, LUA_REGISTRYINDEX, "__LuaStarDisplayThisPtr");
  auto lsd = (LuaStarDisplay*)lua_touserdata(L, -1);
  lsd->setLuaErrMsg(msg);
  return LUA_ERRRUN;
}



LuaStarDisplay::LuaStarDisplay() : L_(nullptr)
{
}


LuaStarDisplay::~LuaStarDisplay()
{ 
  if (L_) Close(); 
}


void LuaStarDisplay::Open()
{ 
  L_ = luaL_newstate();
  lua_pushlightuserdata(L_, wrap_cpp_exceptions);
  lua_pop(L_, 1);
  lua_pushlightuserdata(L_, (void*)(this));
  lua_setfield(L_, LUA_REGISTRYINDEX, "__LuaStarDisplayThisPtr");
  set_pcall_callback(&pcall_error_callback);
  luaL_openlibs(L_); 
  luabind::open(L_);
  luaopen_libGlm(L_);
  luaopen_libRnd(L_);
  luaopen_libWin(L_);
  luaopen_libParam(L_);
  luaopen_libCamera(L_);
  luaopen_libIText(L_);
  luaopen_bit(L_);
}


void LuaStarDisplay::cacheLuaSim()
{
  LuaSim_ = luabind::globals(L_)["Simulation"];
}


void LuaStarDisplay::Close()
{ 
  if (LuaSim_.is_valid()) LuaSim_ = luabind::object();
  if (L_) lua_close(L_); 
  errMsg_.clear();
  L_ = nullptr; 
}


object LuaStarDisplay::LuaSim()
{
  return LuaSim_;
}


void LuaStarDisplay::DoFile(const char* filename)
{
  if ( luaL_dofile(L_, filename) )
  {
    setLuaErrMsg("");
    throw error(L_);
  }
}


bool LuaStarDisplay::ProcessKeyboardHooks(unsigned key, unsigned kbState)
{
  object pkh = globals(L_)["__ProcessKeyboardHooks"];
  return object_cast<bool>(pkh((key << 8) | kbState));
}


bool LuaStarDisplay::ProcessMouseHooks(int x, int y, unsigned button, bool dbl, unsigned kbState)
{
  object pmh = globals(L_)["__ProcessMouseHooks"];
  if (dbl) kbState |= 8;
  return object_cast<bool>(pmh((button << 8) | kbState, x, y));
}


class ITextBox* LuaStarDisplay::CreateTextBox(class IText* self, const char* name, const glm::ivec4& box, const class ICamera* camera)
{
  return libIText::CreateTextBoxCpp(self, name, box, camera);
}


void LuaStarDisplay::setLuaErrMsg(const char* msg)
{
  errMsg_ = msg ? msg : "";
}


