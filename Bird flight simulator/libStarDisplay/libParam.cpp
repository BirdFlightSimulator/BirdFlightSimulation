#include <luabind/luabind.hpp>
#include <luabind/adopt_policy.hpp>
#include "libParam.hpp"
#include "libGLM.hpp"
#include <iostream>

namespace libParam
{
  using namespace Param;
	using namespace luabind;


  template <> Roost LIBSTARDISPLAY_API FromLua<Roost>(const object& luaobj)
  {
    Roost cobj;
    cobj.numPrey = object_cast<unsigned>(luaobj["numPrey"]);
    cobj.numPredators = object_cast<unsigned>(luaobj["numPredators"]);
    cobj.Radius = object_cast<float>(luaobj["Radius"]);
    cobj.minRadius = object_cast<float>(luaobj["minRadius"]);
    cobj.maxRadius = object_cast<float>(luaobj["maxRadius"]);
    return cobj;
  }


  template <> ModelDef LIBSTARDISPLAY_API FromLua<ModelDef>(const object& luaobj)
  {
    ModelDef cobj;
    cobj.name = object_cast<std::string>(luaobj["name"]);
    cobj.acFile = object_cast<std::string>(luaobj["acFile"]);
    cobj.shader = object_cast<std::string>(luaobj["shader"]);
    cobj.id = object_cast<int>(luaobj["id"]);
    cobj.Scale = object_cast<float>(luaobj["Scale"]);
    return cobj;
  }

  template <> Skybox LIBSTARDISPLAY_API FromLua<Skybox>(const luabind::object& luaobj)
  {
    Skybox cobj;
    cobj.name = object_cast<std::string>(luaobj["name"]);
    cobj.ColorCorr = object_cast<glm::vec3>(luaobj["ColorCorr"]);
    cobj.fovy = object_cast<float>(luaobj["fovy"]);
    return cobj;
  }

  template <> RenderFlags LIBSTARDISPLAY_API FromLua<RenderFlags>(const luabind::object& luaobj)
  {
    RenderFlags cobj;
    cobj.show_trails = object_cast<bool>(luaobj["show_trails"]);
    cobj.show_annotation = object_cast<bool>(luaobj["show_annotation"]);
    cobj.show_fps = object_cast<bool>(luaobj["show_fps"]);
    cobj.slowMotion = object_cast<bool>(luaobj["slowMotion"]);
    cobj.helpMsg = object_cast<const char*>(luaobj["helpMsg"]);
	  cobj.turnOffGraphics = object_cast<bool>(luaobj["turnOffGraphics"]);
    return cobj;
  }


  template <> Params LIBSTARDISPLAY_API FromLua<Params>(const luabind::object& luaobj)
  {
    Params cobj;
    luabind::object Sim = luabind::globals(luaobj.interpreter())["Simulation"];
    cobj.DebugLevel = object_cast<int>(Sim["DebugLevel"]);
    cobj.DebugLogOnce = object_cast<bool>(Sim["DebugLogOnce"]);
    cobj.DebugLogStackLevel = object_cast<int>(Sim["DebugLogStackLevel"]);
    object tab = Sim["FSAA"];
    cobj.FSAA[0] = object_cast<int>(tab[1]);
    cobj.FSAA[1] = object_cast<int>(tab[2]);
    cobj.swap_control = object_cast<int>(Sim["swap_control"]);
    cobj.maxPrey = object_cast<unsigned>(Sim["maxPrey"]);
    cobj.maxPredators = object_cast<unsigned>(Sim["maxPredators"]);
    cobj.maxTopologicalRange = object_cast<unsigned>(Sim["maxTopologicalRange"]);
    cobj.roost = FromLua<Roost>(luaobj["Roost"]);
    cobj.IntegrationTimeStep = object_cast<double>(Sim["IntegrationTimeStep"]);
    cobj.slowMotion = object_cast<int>(Sim["slowMotion"]);
    cobj.pausedSleep = object_cast<int>(Sim["pausedSleep"]);
    cobj.realTime = object_cast<bool>(Sim["realTime"]);
    cobj.maxSkippedFrames = object_cast<int>(Sim["maxSkippedFrames"]);
    tab = Sim["Fonts"];
    cobj.TextColor = object_cast<glm::dvec3>(tab["TextColor"]);
    cobj.TextColorAlt = object_cast<glm::dvec3>(tab["TextColorAlt"]);
    for (iterator it(tab["Faces"]), end; it != end; ++it)
    {
      cobj.Fonts.push_back(std::make_pair(object_cast<const char*>(it.key()), object_cast<const char*>(*it)));
    }
    tab = luaobj["Ruler"];
    cobj.RulerTick = object_cast<float>(tab["Tick"]);
    cobj.RulerMinFlockSize = object_cast<unsigned>(tab["MinFlockSize"]);
    cobj.skybox = FromLua<Skybox>(Sim["Skybox"]);
    tab = luaobj["Trail"];
    cobj.TrailLength = object_cast<double>(tab["Length"]);
    cobj.TrailWidth = object_cast<float>(tab["Width"]);
    cobj.TrailTickInterval = object_cast<float>(tab["TickInterval"]);
    cobj.TrailTickWidth = object_cast<float>(tab["TickWidth"]);
    cobj.TrailSkip = object_cast<int  >(tab["Skip"]);
    for (iterator it(Sim["ModelSet"]), end; it != end; ++it)
    {
      cobj.ModelSet.push_back(FromLua<ModelDef>(*it));
    }

    cobj.renderFlags = FromLua<RenderFlags>(luaobj["RenderFlags"]);
	  cobj.evolution.durationGeneration = object_cast<float  >(luaobj["evolution"]["durationGeneration"]);
	  cobj.evolution.startGen = object_cast<int >(luaobj["evolution"]["startGen"]);
	  cobj.evolution.load = object_cast<bool>(luaobj["evolution"]["load"]);
	  cobj.evolution.loadFolder = object_cast<std::string>(luaobj["evolution"]["loadFolder"]);
	  cobj.evolution.type = object_cast<std::string  >(luaobj["evolution"]["type"]);
	  cobj.evolution.fileName = object_cast<std::string  >(luaobj["evolution"]["fileName"]);
	  cobj.evolution.Trajectories.amount = object_cast<int>(luaobj["evolution"]["Trajectories"]["amount"]);
	  cobj.evolution.Trajectories.dt = object_cast<float>(luaobj["evolution"]["Trajectories"]["dt"]);
	  cobj.evolution.terminationGeneration = object_cast<int  >(luaobj["evolution"]["terminationGeneration"]);
	  cobj.evolution.externalPreyFile = object_cast<std::string  >(luaobj["evolution"]["externalPreyFile"]);
	  cobj.evolution.oneOnOnePredPrey = object_cast<bool  >(luaobj["evolution"]["oneOnOnePredPrey"]);
	  cobj.evolution.stationaryPrey = object_cast<bool>(luaobj["evolution"]["stationaryPrey"]);
	  cobj.evolution.artificialPrey = object_cast<bool>(luaobj["evolution"]["artificialPrey"]);
	  cobj.evolution.artificialPredator = object_cast<bool>(luaobj["evolution"]["artificialPredator"]);
	  cobj.evolution.title = object_cast<std::string>(luaobj["evolution"]["title"]);
	  cobj.DataStorage.folder = object_cast<std::string >(luaobj["DataStorage"]["folder"]);

	  cobj.birds.csv_file_species = object_cast<std::string  >(luaobj["Birds"]["csv_file_species"]);
	  cobj.birds.csv_file_prey_predator_settings = object_cast<std::string  >(luaobj["Birds"]["csv_file_prey_predator_settings"]);


	  return cobj;
  }


  template <> luabind::object LIBSTARDISPLAY_API ToLua<Roost>(lua_State* L, const Roost& cobj)
  {
    object luaobj = newtable(L);
    luaobj["numPrey"] = cobj.numPrey;
    luaobj["numPredators"] = cobj.numPredators;
    luaobj["Radius"] = cobj.Radius;
    luaobj["minRadius"] = cobj.minRadius;
    luaobj["maxRadius"] = cobj.maxRadius;
    return luaobj;
  }


  template <> luabind::object LIBSTARDISPLAY_API ToLua<Skybox>(lua_State* L, const Skybox& cobj)
  {
    object luaobj = newtable(L);
    luaobj["name"] = cobj.name;
    luaobj["ColorCorr"] = cobj.ColorCorr;
    luaobj["fovy"] = cobj.fovy;
    return luaobj;
  }

  
  template <> luabind::object LIBSTARDISPLAY_API ToLua<RenderFlags>(lua_State* L, const RenderFlags& cobj)
  {
    object luaobj = newtable(L);
    luaobj["show_trails"] = cobj.show_trails;
    luaobj["show_annotation"] = cobj.show_annotation;
    luaobj["show_fps"] = cobj.show_fps;
    luaobj["slowMotion"] = cobj.slowMotion;
    luaobj["helpMsg"] = cobj.helpMsg;
  	luaobj["turnOffGraphics"] = cobj.turnOffGraphics;
    return luaobj;
  }

  double GetBodyMass(Bird& bird)
  {
    return bird.bodyMass;
  }

  double GetwingMass(Bird& bird)
  {
	  return bird.wingMass;
  }

  void SetBodyMass(Bird& bird, double newVal)
  {
    bird.bodyMass = float(newVal);
    bird.bodyWeight = float(9.81 * newVal);
  }

  void SetwingMass(Bird& bird, double newVal)
  {
	  bird.wingMass = float(newVal);
  }

}


void luaopen_libParam(lua_State* L)
{
  using namespace luabind;
  using namespace Param;

  module(L, "Params")[


    class_<Bird>("Bird")
      .def(constructor<>())
      .def_readwrite("reactionTime", &Bird::reactionTime)
      .property("bodyMass", &libParam::GetBodyMass, &libParam::SetBodyMass)
	    .property("wingMass", &libParam::GetwingMass, &libParam::SetwingMass)
      .def_readwrite("rho", &Bird::rho)
	    .def_readwrite("bodyWeight", &Bird::bodyWeight)
      .def_readwrite("wingSpan", &Bird::wingSpan)
	    .def_readwrite("InertiaWing", &Bird::InertiaWing)
	    .def_readwrite("InertiaBody", &Bird::InertiaBody)
	    .def_readwrite("J", &Bird::J)
      .def_readwrite("wingAspectRatio", &Bird::wingAspectRatio)
	    .def_readwrite("wingBeatFreq", &Bird::wingBeatFreq)
	    .def_readwrite("birdName", &Bird::birdName)
	    .def_readwrite("theta", &Bird::theta)
	    .def_readwrite("generation", &Bird::generation)
	    .def_readwrite("wingLength", &Bird::wingLength)
	    .def_readwrite("bodyArea", &Bird::bodyArea)
	    .def_readwrite("cBody", &Bird::cBody)
	    .def_readwrite("cFriction", &Bird::cFriction)
	    .def_readwrite("constRollAcc", &Bird::constRollAcc)
	    .def_readwrite("L0", &Bird::L0)
	    .def_readwrite("contMaxLift", &Bird::contMaxLift)
	    .def_readwrite("oscillations", &Bird::oscillations)
	    .def_readwrite("maneuver", &Bird::maneuver)
      .def_readwrite("wingArea", &Bird::wingArea)
      .def_readwrite("CL", &Bird::CL)
      .def_readwrite("cruiseSpeed", &Bird::cruiseSpeed)
      .def_readwrite("speedControl", &Bird::speedControl)
      .def_readwrite("blindAngle", &Bird::blindAngle)
	    .def_readwrite("InitialPosition", &Bird::InitialPosition)
	    .def_readwrite("InitialSpeed", &Bird::InitialSpeed)
	    .def_readwrite("TopSpeed", &Bird::TopSpeed)
	    .def_readwrite("errorInControl", &Bird::errorInControl)
	    .def_readwrite("errorInRoll", &Bird::errorInRoll)
	    .def_readwrite("NoIndDrag", &Bird::NoIndDrag)
	    .def_readwrite("InitialHeading", &Bird::InitialHeading)
      .def_readwrite("altitude", &Bird::altitude),

    class_<Prey>("Prey")
      .def(constructor<>())
      .def_readwrite("EvasionStrategyTEMP", &Prey::EvasionStrategyTEMP)
	    .def_readwrite("smoothManeuverParam", &Prey::smoothManeuverParam)
	    .def_readwrite("nonSmoothManeuverParam", &Prey::nonSmoothManeuverParam)
	    .def_readwrite("startingPoints", &Prey::startingPoints)
	    .def_readwrite("endPoints", &Prey::endPoints)
	    .def_readwrite("fleeDirection", &Prey::fleeDirection)
	    .def_readwrite("y_evade", &Prey::y_evade)
	    .def_readwrite("z_evade", &Prey::z_evade)
	    .def_readwrite("noManeuverParam", &Prey::noManeuverParam)
	    .def_readwrite("artificial", &Prey::artificial),
    

	  class_<Trajectory>("Trajectory")
	    .def(constructor<>())
	    .def_readwrite("Pred_acc", &Trajectory::Pred_acc)
	    .def_readwrite("Pred_forward", &Trajectory::Pred_forward)
	    .def_readwrite("Pred_gen", &Trajectory::Pred_gen)
	    .def_readwrite("Pred_id", &Trajectory::Pred_id)
	    .def_readwrite("Pred_position", &Trajectory::Pred_position)
	    .def_readwrite("Pred_up", &Trajectory::Pred_up)
	    .def_readwrite("Pred_velocity", &Trajectory::Pred_velocity)
	    .def_readwrite("Prey_acc", &Trajectory::Prey_acc)
	    .def_readwrite("Prey_forward", &Trajectory::Prey_forward)
	    .def_readwrite("Prey_forACC", &Trajectory::Prey_forACC)
	    .def_readwrite("Prey_latACC", &Trajectory::Prey_latACC)
	    .def_readwrite("Prey_id", &Trajectory::Prey_id)
	    .def_readwrite("N", &Trajectory::N)
	    .def_readwrite("Prey_position", &Trajectory::Prey_position)
	    .def_readwrite("Prey_up", &Trajectory::Prey_up)
	    .def_readwrite("Prey_velocity", &Trajectory::Prey_velocity)
	    .def_readwrite("Prey_roll_acc", &Trajectory::Prey_roll_acc)
	    .def_readwrite("Prey_roll_rate", &Trajectory::Prey_roll_rate)
	    .def_readwrite("Pred_roll_acc", &Trajectory::Pred_roll_acc)
	    .def_readwrite("Pred_desLatACC", &Trajectory::Pred_desLatACC)
	    .def_readwrite("Pred_latACC", &Trajectory::Pred_latACC)
	    .def_readwrite("Pred_roll_rate", &Trajectory::Pred_roll_rate)
	    .def_readwrite("time", &Trajectory::time),

    class_<Predator>("Predator")
      .def(constructor<>())
  	  .def_readwrite("PursuitStrategy", &Predator::PursuitStrategy)
      .def_readwrite("StartAttack", &Predator::StartAttack)
      .def_readwrite("PreySelection", &Predator::PreySelection)
      .def_readwrite("CatchDistance", &Predator::CatchDistance)
	    .def_readwrite("StoreTrajectory", &Predator::StoreTrajectory)
	    .def_readwrite("VisualError", &Predator::VisualError)
	    .def_readwrite("VisualBias", &Predator::VisualBias)
	    .def_readwrite("artificial", &Predator::artificial)
	    .def_readwrite("DPAdjParam", &Predator::DPAdjParam)
	    .def_readwrite("N", &Predator::N)
	    .def_readwrite("N2", &Predator::N2)
		.def_readwrite("filter", &Predator::filter)
  ];
}

