
local evolution = {}



function evolution.evolve_next()

    return function(expNum)

	    begin = os.clock()
	    local Pred_params = {}
	    local Bird_params = {}
	    fitness = {}
	    counter = 0
	    local Generation = Simulation.Generation()
	    print("Generation = " .. Generation)

	    crit = experiments[expNum].Param.evolution.pred_fitness_criterion
	   
		for p in Simulation.Predators() do
			counter = counter + 1
			Pred_params[counter] = p.PredParams
			Bird_params[counter] = p.BirdParams
			Bird_params[counter].generation = Generation
			pred_stat = p:GetHuntStat()
			if crit ~= nil then
				loadstring("fitness[counter] = {" .. crit .. " , counter}")()
				if (string.find(tostring(fitness[counter][1]),"#IND")) then fitness[counter][1] = 999 end
				if (string.find(tostring(fitness[counter][1]),"#QNAN")) then fitness[counter][1] = 999 end
			end
		end

		if (Generation == 1) then 
			initialize_evolving_parameters(Pred_params, "pred_params",expNum) 
			initialize_evolving_parameters(Bird_params, "predBird_params",expNum) 
		end
		if crit ~= nil then
			Pred_params, fitness = sort_by(Pred_params, fitness)
			Bird_params, fitness = sort_by(Bird_params, fitness)
		end
		copy_half_and_mutate(Pred_params, "pred_params", expNum)
		copy_half_and_mutate(Bird_params, "predBird_params", expNum)
		randomize_random_variables(Pred_params, "pred_params", expNum)
		randomize_random_variables(Bird_params, "predBird_params", expNum)


	    Prey_params = {}
	    Bird_params = {}
	    fitness = {}
        counter = 0
	    crit = experiments[expNum].Param.evolution.prey_fitness_criterion
	   
		for p in Simulation.Prey() do
			counter = counter + 1
			Prey_params[counter] = p.PreyParams
			Bird_params[counter] = p.BirdParams
			Bird_params[counter].generation = Generation
			
			prey_stat = p.BirdParams
			the_prey = p
			if crit ~= nil then
				loadstring("fitness[counter] = {" .. crit .. " , counter}")()
				if (string.find(tostring(fitness[counter][1]),"#IND")) then fitness[counter][1] = 999 end
				
			end
		end

		if (Generation == 1) then 
			initialize_evolving_parameters(Prey_params, "prey_params",expNum) 
			initialize_evolving_parameters(Bird_params, "preyBird_params",expNum) 
		end

		if crit ~= nil then
			Prey_params, fitness = sort_by(Prey_params, fitness)
			Bird_params, fitness = sort_by(Bird_params, fitness)
		end

		counter = 0
		total = 0
		for fit, val in pairs(fitness) do
		    counter = counter + 1
		    print(fitness[counter][1] .. " = fitness " .. " s1: ".. round2(Prey_params[counter].startingPoints.x,2) .. "  s2: " .. round2(Prey_params[counter].startingPoints.y,2) .. "  s3: " .. round2(Prey_params[counter].startingPoints.z,2) .. " s4: " .. round2(Prey_params[counter].startingPoints.w,2) .. "  e1: " .. round2(Prey_params[counter].endPoints.x,2) .. " e2: " .. round2(Prey_params[counter].endPoints.y,2) .. " e3: " .. round2(Prey_params[counter].endPoints.z,2) .. " e4: " .. round2(Prey_params[counter].endPoints.w,2) .. "  z1: " .. round2(Prey_params[counter].z_evade.x,2) .. " z2: " .. round2(Prey_params[counter].z_evade.y,2) .. " z3: " .. round2(Prey_params[counter].z_evade.z,2) .. " z4: " .. round2(Prey_params[counter].z_evade.w,2))
		    total = total + fitness[counter][1]
		end
		print("total fitness: " .. total / counter)

		copy_half_and_mutate(Prey_params, "prey_params", expNum)
		copy_half_and_mutate(Bird_params, "preyBird_params", expNum)
		randomize_random_variables(Prey_params, "prey_params", expNum)
		randomize_random_variables(Bird_params, "preyBird_params", expNum)

	   
	    print("Evolution in seconds: " .. os.clock() - begin)
	end

	--next generation
	--sort on fitness criterium
	-- duplicate first half to second half
	-- random mutation
	-- randomize random variables


end




return evolution