print("  sourcing Birds.lua")
-- Birds.lua
--
-- A function that loads the birds excel sheets and chooses the appropriate bird settings for prey or predator

local random = require "Random"
require "helper_functions"


local Birds = {
}



function Birds.newBird (p, file,settingsFile, name, isPredator, gender)
		  local bird = {}
		  local file = io.open(file, "r")
		  local names = ParseCSVLine(file:read(),",")
		  local bird_info = {}
		  local tmp = {}
		  local prey_pred = 0
		  local bird_spec = 0
		  local line = ParseCSVLine(file:read(),",")
		  local predator = {}
		  local prey = {}
		  while line[2] ~= "" do  
			if(line[2] == name) then
				tmp = line
				for k,v in pairs(tmp) do		  
				  bird_info[names[k]] = tmp[k]
				end
			end
			line = ParseCSVLine(file:read(),",")
		  end
		  io.close(file)
		  local keyset={}
		  local n=0
		  local settings = io.open(settingsFile, "r")
		  line = ParseCSVLine(settings:read(),",")
		  while line[1] ~= "end" do  
			if line[1] == "Prey" then prey_pred = 1 end
			if line[1] == "Predator" then prey_pred = 2 end
			if line[1] == "Bird Properties" then bird_spec = 1 end
			if line[1] == "Prey Properties" then bird_spec = 2 end
			if line[1] == "Predator Properties" then bird_spec = 3 end
			if isPredator == 1 and prey_pred == 2  and bird_spec == 1 and line[1] ~= "Predator Properties" and line[1] ~= "" then
			  bird[line[1]] = process_CSV_cell(line[2], line[3])
			end
			if isPredator == 1 and prey_pred == 2  and bird_spec == 3 and line[1] ~= "Bird Properties" and line[1] ~= "" then
			  predator[line[1]] = process_CSV_cell(line[2], line[3])
			end
			if isPredator == 0 and prey_pred == 1  and bird_spec == 1 and line[1] ~= "Bird Properties" and line[1] ~= "" then
			  bird[line[1]] = process_CSV_cell(line[2], line[3])
			end
			if isPredator == 0 and prey_pred == 1  and bird_spec == 2 and line[1] ~= "Prey Properties" and line[1] ~= "" then
			  if  line[1] ~= "EvasionStrategy" then 
				prey[line[1]] = process_CSV_cell(line[2], line[3]) 
			  end
			end
			line = ParseCSVLine(settings:read(),",")
		  end
		  io.close(settings)
		  prey.EvasionStrategyTEMP = 0

		  --used: physical
		  bird.rho = 1.2
		  bird.bodyMass = tonumber(bird_info["body mass " .. gender]) / 1000     -- [kg]
		  bird.wingMass = tonumber(bird_info["wing mass " .. gender])   
		  bird.InertiaWing = tonumber(bird_info["moment of inertia extended wing " .. gender])
		  bird.InertiaBody = tonumber(bird_info["moment of inertia body " .. gender])
		  bird.J = tonumber(bird_info["J wing " .. gender])
		  bird.wingSpan = tonumber(bird_info["Wingspan " .. gender] / 100  )      -- [m]
		  bird.wingAspectRatio = tonumber(bird_info["Aspect ratio " .. gender])
		  bird.wingBeatFreq = tonumber(bird_info["Wingbeat frequency " .. gender])
		  bird.birdName = string.gsub(bird_info["Species(eng)"], " ", "_")
		  bird.theta =  tonumber(bird_info["Span Angle Down to Upstroke " .. gender])
		  bird.wingLength = tonumber(bird_info["Wing length " .. gender]) / 100
		  bird.bodyArea = tonumber(bird_info["Body area " .. gender] )
		  bird.cBody =  tonumber(bird_info["Body drag coefficient " .. gender] )
		  bird.cFriction = tonumber(bird_info["Friction drag wing " .. gender])
		  bird.wingArea = bird.wingSpan * (bird.wingSpan / bird.wingAspectRatio)   -- [m^2] 
		  bird.cruiseSpeed = tonumber(bird_info["Cruise speed " .. gender])
		  bird.L0 = 1.7 * bird.bodyMass *9.81;

		  --intitial pred parameters
		  bird.InitialPosition = {x = 0, y = 0, z = 0}
		  bird.InitialHeading = {x = 1, y = 0, z = 0}
		  bird.InitialSpeed = tonumber(bird_info["Cruise speed male"])
		  bird.TopSpeed = (math.pi^3/8*(bird.wingAspectRatio*bird.wingArea*bird.wingBeatFreq^2*bird.cruiseSpeed*bird.wingLength^2*math.sin(bird.theta/2)^2)/(bird.wingArea*bird.cFriction + bird.bodyArea*bird.cBody))^(1/3)
		  bird.errorInControl = 0
		  bird.errorInRoll = 0
		  bird.NoIndDrag = false
		  bird.constRollAcc = 0
		  bird.contMaxLift = 0
		  bird.oscillations = 0
		  predator.DPAdjParam = 0
		  predator.N = 3
		  predator.N2 = 1
		  predator.filter = 0
		  bird.generation = 0
		  predator.artificial ={x = 0, y = 0, z =0}

		  -- initialize prey parameters
		  prey.smoothManeuverParam = {x = 2, y = 3.7, z =0.9, w = 1}
		  prey.nonSmoothManeuverParam = {x = 10, y = 1, z =2, w = 0.1}
		  prey.fleeDirection = {x = 0, y = 0, z =0}
		  prey.z_evade = {x = 0.54, y = -8.87, z =-0.11, w = 1.22}
		  prey.y_evade = {x = 0, y = 0, z =0}
		  prey.startingPoints = {x = 18.95, y = 1.72, z = 4.08, w = 4.87}
		  prey.endPoints = {x = 3.29, y = 0.04, z = -0.52, w = 2.16}
		  prey.noManeuverParam = {x = 1, y = 1, z =1, w = 1}
		  prey.artificial ={x = 0, y = 0, z =0}
		  -- currently unused, to be deleted or may we useful later
		  bird.controlCL = false
		  bird.maneuver = 1



		  if isPredator == 0 then
		     cBird = Params.Bird()
			 cPrey = Params.Prey()
			 tableToUserData(bird, "cBird")
		     tableToUserData(prey, "cPrey")
			 if p ~= nil then
				 p.BirdParams = cBird
				 p.PreyParams = cPrey
			 end
			 return cBird, cPrey, bird, prey
		  else
		     cBird = Params.Bird()

		     cPred = Params.Predator()
			 tableToUserData(bird, "cBird")
		     tableToUserData(predator, "cPred")
			 cPred.ExposureThreshold = glm.vec2(3,5)

			 if p ~= nil then
				 p.BirdParams = cBird
				 p.PredParams = cPred
			 end
			 return cBird, cPred, bird, predator
		  end
end

function Birds.getMorphology(birdy)
--This function extracts the relevent  morphological data from an userData to a lua table
		  bird = {}
		  bird.bodyMass =birdy.bodyMass 
		  bird.wingMass =birdy.wingMass
		  bird.InertiaWing = birdy.InertiaWing
		  bird.InertiaBody =  birdy.InertiaBody
		  bird.J =birdy.J
		  bird.wingSpan = birdy.wingSpan     -- [m]
		  bird.L0 = birdy.L0
		  bird.wingAspectRatio =birdy.wingAspectRatio 
		  bird.wingBeatFreq = birdy.wingBeatFreq
		  bird.birdName = tostring (birdy.birdName)
		  bird.theta =  birdy.theta
		  bird.wingLength = birdy.wingLength
		  bird.bodyArea = birdy.bodyArea
		  bird.cBody =  birdy.cBody
		  bird.cFriction = birdy.cFriction 
		  bird.wingArea = birdy.wingArea -- [m^2] 
		  bird.cruiseSpeed = birdy.cruiseSpeed
		  return bird
end



function TrajectoryTable()
   
   local trajectory = {
    Pred_position = {x=0,y=0,z=0},
	Prey_position= {x=0,y=0,z=0},
	Pred_up= {x=0,y=0,z=0},
	Prey_up= {x=0,y=0,z=0},
	Pred_forward= {x=0,y=0,z=0},
	Prey_forward= {x=0,y=0,z=0},
	Pred_acc= {x=0,y=0,z=0},
	Pred_latACC = 0,
	Pred_desLatACC = 0,
	Prey_acc= {x=0,y=0,z=0},
	Pred_velocity= {x=0,y=0,z=0},
	Prey_velocity= {x=0,y=0,z=0},
	Prey_forACC = 0,
	Prey_latACC = 0,
	Pred_roll_rate = 0;
	Pred_roll_acc = 0;
	Prey_roll_rate = 0;
	Prey_roll_acc = 0;
	Pred_id =0,
	Pred_gen =0,
	Prey_id =0,
	N = 0,
	time =0,
   
   }
   return trajectory
end


return Birds

