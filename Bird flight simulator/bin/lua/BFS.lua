print("  sourcing BFS.lua")

-- some limits
-- 
Simulation.maxPrey = 50000
Simulation.maxPredators = 10000
Simulation.maxTopologicalRange = 16     -- memory optimization, keep it small


Simulation.DebugLevel = 0
Simulation.DebugLogOnce = true
Simulation.DebugLogStackLevel = 0


Simulation.IntegrationTimeStep = 0.001  -- [s]
Simulation.slowMotion = 10              -- slowdown factor
Simulation.pausedSleep = 0              -- sleep time if paused [microseconds]
Simulation.realTime = true
Simulation.maxSkippedFrames = 5


-- Full screen antialiasing modes
local FSAAModes = {
--Name = { color samples, coverage samples }  
  Off = { 0, 0 }, 
  MSAA4 = { 4, 0 },
  MSAA8 = { 8, 0 },
  CSAA8x = { 4, 8 },
  CSAA8xQ = { 8, 8 },
  CSAA16x = { 4, 16 },
  CSAA16xQ = { 8, 16 },
}


Simulation.FSAA = FSAAModes.CSAA8x
Simulation.swap_control = 0             -- see WGL_EXT_swap_control


Simulation.Fonts = {
  Faces = {
    smallface = "Verdana14.fnt",
    mediumface = "Verdana16.fnt",
    bigface = "Verdana18.fnt",
    monoface = "Lucida12.fnt",
	superlarge = "Verdana32.fnt",
	superlargeBold = "Verdana32Bold.fnt"
  },
  TextColor = glm.vec3(0.18, 0.18, 0.18),
  TextColorAlt = glm.vec3(0.75, 0.75, 0.75),
}


Simulation.Skybox = {
  name = "BlueishSky",
  --name = "GraySky",
  ColorCorr = glm.vec3(1.0, 1.0, 1.0),
  fovy = 45.0
}


Simulation.ModelSet = {
  --{ name = "delta", acFile = "delta.ac", Scale = 1, id = 1; shader = "StaticInstancing" }, 
  --{ name = "delta", acFile = "delta.ac", Scale = 1, id = 1; shader = "WingInstancing" },
  --{ name = "delta", acFile = "delta.ac", Scale = 2, id = 1; shader = "StaticInstancing" }, 
  --{ name = "delta", acFile = "delta.ac", Scale = 2, id = 1; shader = "WingInstancing" }, 
  { name = "body", acFile = "starling_body_wing.ac", Scale = 1, id = 1; shader = "StaticInstancing" }, 
  { name = "wings", acFile = "starling_body_wing.ac", Scale = 1, id = 2; shader = "WingInstancing" },
  { name = "body", acFile = "mesh_Peregrine_body_underskinned_tmp.ac", Scale = 1, id = 1; shader = "StaticInstancing" }, 
  { name = "Wing", acFile = "scaled_body_wing_peregrine.ac", Scale = 1, id = 2; shader = "WingInstancing" }, 
}


-- Bootstraped from BFS.exe
--
function Simulation.Initialize (ExePath, ConfigFile)
  local sim = Simulation
  sim.Version = "1.0"
  sim.WorkingPath = ExePath .. "\\..\\..\\"
  sim.ExePath = ExePath
  sim.LuaPath = sim.WorkingPath .. "lua\\"
  sim.DataPath = sim.WorkingPath .. "data\\"
  sim.MediaPath = sim.WorkingPath .. "media\\"
  sim.ShaderPath = sim.WorkingPath .. "shader\\"
  sim.ConfigFile = sim.WorkingPath .. (ConfigFile or "config.lua")
  package.path = package.path .. ";" .. sim.LuaPath .. "?.lua"
  package.path = package.path .. ";" .. sim.WorkingPath .. "?.lua"
  dofile(sim.LuaPath .. "Help.lua")
  dofile(sim.LuaPath .. "KeyHooks.lua")
  dofile(sim.LuaPath .. "MouseHooks.lua")
  random:seed(os.time())
end


function InitHook ()
  print("\nFunction InitHook not defined.")
  return true
end


function Simulation.CheckVersion (Version)
  local sim = Simulation;
  if sim.Version ~= Version then
    error("Version mismatch: expecting " .. sim.Version .. " got " .. Version, 2)
  end
end


function Simulation.OutputFileName (mapping)
  local sim = Simulation
  local t = os.date("_%d-%m-%y_%H-%M-%S")
  if type(mapping) == "number" then
    mapping = ReverseFMappings[mapping]
  end
  local file = sim.DataPath .. mapping .. tostring(t) .. ".dat"
  return { name = file, append = false }
end


