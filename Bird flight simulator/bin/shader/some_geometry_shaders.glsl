

//a ribbon shader for particles
shader[vertex] Ribbon
{
  layout (location = 0) in vec4 side_T;       // vSide vector & simulation time
  layout (location = 1) in vec4 pos_U;        // position & color texture coord
  layout (location = 2) in float RS; 
  layout (location = 3) in float beatCycle;
  layout (location = 4) in vec4 up_s;       // vSide vector & simulation time

  out float vColorTexCoord;    // color texture
  out float vTime;             // Time
  out vec3 vPosition;
  out vec3 vSide;              // Side vector
  out vec3 vUp;
  out float vBeatCycle;
  out float vRS;

  void main(void)
  {
    vTime = side_T.w;
    vPosition = pos_U.xyz; // + mod(beatCycle / (2.0f*3.14f),1);
    vColorTexCoord = pos_U.w;
    vSide = side_T.xyz  * up_s.w;
	vUp = up_s.xyz;
	vBeatCycle = beatCycle;
	vRS = RS;
  }
};


shader[geometry] Ribbon
{
  layout(lines_adjacency) in;
  layout(points, max_vertices = 160) out;

  const vec4 eye = vec4(0.0, 0.0, 1.0, 0.0);
  const float diffuse = 0.5;
  const float ambient = 1.0 - diffuse;

  uniform sampler1D colorTex;
  uniform float halfWidth;

  in float vTime[4];              // time
  in float vColorTexCoord[4];     // color texture
  in vec3 vPosition[4];           // position
  in vec3 vSide[4];               // vSide vector
  in vec3 vUp[4];
  in float vBeatCycle[4];
  in float vRS[4];

  smooth out vec4 gColor;
  smooth out float gSimTime;
  smooth out float gShade;

  float shade(vec3 normal)
  {
    float ds = abs(dot(ModelView * vec4(normal, 0.0), eye));
    return diffuse * ds + ambient;
  }

  void Emit(vec3 v)
  {
    gl_Position = ModelViewProjection * vec4(v, 1.0);
    EmitVertex();
  }

  float rand(float n){return fract(sin(n) * 43758.5453123);}


  void main(void)
  {
    float skip = vTime[0] * vTime[1] * vTime[2] * vTime[3];
    if (skip == 0) return;

    vec3 tangent01 = normalize(vPosition[1] - vPosition[0]);
    vec3 tangent21 = normalize(vPosition[2] - vPosition[1]);
    vec3 tangent23 = normalize(vPosition[3] - vPosition[2]);

    vec3 up0 = cross(vSide[0], tangent01);
    vec3 up1 = cross(vSide[1], tangent21);
    vec3 up2 = cross(vSide[2], tangent23);

    float shade0 = shade(up0);
    float shade1 = shade(up1);
    float shade2 = shade(up2);

    vec4 color0 = texture(colorTex, vColorTexCoord[0]);
    vec4 color1 = texture(colorTex, vColorTexCoord[1]);
    vec4 color2 = texture(colorTex, vColorTexCoord[2]);

	float amplitude = 0.78f;
    gColor = mix(color0, color1, 0.5);

    gShade = mix(shade0, shade1, 0.5);
    gSimTime = vTime[1];

	float t = 0.75f;
	float bone_connect = 1.0f/5.0f;
	float psweep = 0.0;
	
    float length_sec2 = sqrt(1 + psweep*psweep);
    float hoek = asin(vRS[1] / length_sec2);
    float sweep_ = cos(hoek) * length_sec2;
    sweep_ = 0.5*(sweep_ - psweep) + psweep;
    float x_add = bone_connect * psweep / 2 - 0.02;
    float y_add = pow(bone_connect,1.2) * (sweep_ - psweep)*0.5;
    
    float sweep = 1.25*abs(abs(halfWidth) - bone_connect)*float(abs(halfWidth)>bone_connect)*(sweep_ - psweep) - x_add;
	sweep += pow(abs(abs(halfWidth) - bone_connect),1.2)*psweep;
    
    float sweep_y = pow(abs(abs(halfWidth) - bone_connect),1.2)*(0.5*float(abs(halfWidth)>bone_connect) +0.5)*(sweep_ - psweep) - y_add;

	vec3 forward = cross(vUp[1], vSide[1]);

	float wingSec = vPosition[1].z - max(min(vPosition[1].z, 0.06), -0.06);
	//Emit(vPosition[1]);

	
	
	for (int n = 0; n<160; n++)
	{
	    gColor = vec4(1,1,1,0.4) * (1.0f - 0.2f*rand(n + vRS[1]));
	    float particle = halfWidth * (n/80.0f - 1.0f);
		Emit(vPosition[1] - particle * vRS[1] *vSide[1]*cos(amplitude*cos(vBeatCycle[1])) + abs(particle) * vRS[1] * vUp[1]*sin(amplitude*cos(vBeatCycle[1])) - forward * rand(n + vBeatCycle[1] + vRS[1])*0.1f);
	}

    //Emit(vPosition[1] - halfWidth * vRS[1] *(vSide[1]*cos(amplitude*cos(vBeatCycle[1])) - vUp[1]*sin(amplitude*cos(vBeatCycle[1]))));
    //Emit(vPosition[1] + halfWidth * vRS[1] *  (vSide[1]*cos(amplitude*cos(vBeatCycle[1])) + vUp[1]*sin(amplitude*cos(vBeatCycle[1]))));

    gColor = mix(color1, color2, 0.5);
	gColor.a = 0.4;
    gShade = mix(shade1, shade2, 0.5);
    gSimTime = vTime[2];
	//Emit(vPosition[2]);

Emit(vPosition[2] - halfWidth * vRS[2] *  (vSide[2]*cos(amplitude*cos(vBeatCycle[2])) - vUp[2]*sin(amplitude*cos(vBeatCycle[2]))));
    
 Emit(vPosition[2]);
	Emit(vPosition[2] + halfWidth * vRS[2] *  (vSide[2]*cos(amplitude*cos(vBeatCycle[2])) + vUp[2]*sin(amplitude*cos(vBeatCycle[2]))));
   
	
	
  }
};