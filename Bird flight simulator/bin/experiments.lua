--Use this file to setup experiments
local Birds = require "Birds"
local inspect = require "inspect2"
require "helper_functions"

cBird, cPrey, preyBird, prey = Birds.newBird(nil, gParam.Birds.csv_file_species , gParam.Birds.csv_file_prey_predator_settings,"Common starling", 0, "male")
cBird, cPred, predBird, pred = Birds.newBird(nil, gParam.Birds.csv_file_species , gParam.Birds.csv_file_prey_predator_settings,"Peregrine falcon", 1, "male")

experiments = {}

local newExp = function (a)
    local f_species = gParam.Birds.csv_file_species
    local f_settings = gParam.Birds.csv_file_prey_predator_settings
    local starling_bird,  starling_prey = Birds.newBird(nil, f_species , f_settings,"Common starling", 0, "male")
    local peregrine_bird,  peregrine_prey = Birds.newBird(nil,f_species , f_settings,"Peregrine falcon", 0, "male")
    local defaultPrey_bird, defaultPrey_prey = Birds.newBird(nil, f_species , f_settings,"Common starling", 0, "male")
    local defaultPred_bird, defaultPred_pred = Birds.newBird(nil, f_species, f_settings,"Peregrine falcon", 1, "male")

	local experiment = 
	{
		Param = deepcopy(gParam),
		preyBird = defaultPrey_bird,
		predBird = defaultPred_bird,
		prey = defaultPrey_prey,
		pred = defaultPred_pred,
	}
	experiment.pred.PursuitStrategy = 5
	experiment.Param.evolution.terminationGeneration = 50
	experiment.Param.evolution.durationGeneration = 40
	experiment.predBird = peregrine_bird
	experiment.preyBird = starling_bird
	experiment.Param.evolution.Trajectories.dt = 0.01
	experiment.predBird.InitialSpeed = 16
	experiment.predBird.constRollAcc = 0
	experiment.predBird.contMaxLift = 0
	experiment.predBird.oscillations = 0
	experiment.preyBird.InitialSpeed = 12
	experiment.pred.N2 = 1
	experiment.predBird.errorInControl = 0
	experiment.predBird.errorInRoll = 0
	experiment.predBird.reactionTime = 0.05
	experiment.Param.evolution.oneOnOnePredPrey = true
	experiment.pred['VisualError'] = 0
	experiment.pred.N = 3
	experiment.preyBird.maneuver = 1
	experiment.Param.evolution.fileName = "test"
	experiment.Param.evolution.pred_fitness_criterion = nil
	experiment.Param.evolution.prey_fitness_criterion = nil

	experiment.predBird.InitialPosition.y = 200
	experiment.predBird.InitialPosition.x = 50
	experiment.predBird.InitialPosition.z = 0
	
	experiment.Param.evolution.random_variables = {
		  {name = "preyBird_params.reactionTime",  type = "normal", a = 0.07, b = 0.001},
          {name = "preyBird_params.InitialHeading",  type = "vec_in_sphere", template = "{x=0,y=0,z=0}"},
    --Use for experiments of Mills(2018)
	--	  {name = "predBird_params.InitialPosition.y",  type = "uniform", a = -100, b = 2000},
	--	  {name = "predBird_params.InitialPosition.x",  type = "uniform", a = 0, b = 800},
	--	  {name = "pred_params.N",  type = "uniform", a = 1, b = 20},
	}
	return experiment
end


thecounter = 0
for n = 1,1,1 do
	print(n)
	thecounter = thecounter + 1
    experiments[thecounter] = newExp(n)
end
