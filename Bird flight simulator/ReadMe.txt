BirdFlightSimulator first used in "Physics-based simulations of aerial attacks by peregrine falcons reveal that stooping at high speed maximizes catch success against agile prey"
written by: Hanno Hildenbrandt & Robin Mills 2014-2018

note: The repository will be continuously updated with more elaborate readMes. For more information, email r.mills@rug.nl

Installation:
The simulator requires Visual Studio 2017 or later and an NVIDIA graphics card and Windows 7 or higher.
Open BirdFlightSimulator.sln and build in debug or release mode (64 bit).

Usage:
To conduct a new experiment, alter the LUA scripts bin\config.lua and bin\experiments.lua
The main model components are found in StarDisplay\bird.cpp, StarDisplay\predator.cpp, StarDisplay\prey.cpp


