//! \defgroup Model The starling model implementation
//! \file Prey.hpp The 'prey'
//! \ingroup Model

#ifndef PREY_HPP_INCLUDED
#define PREY_HPP_INCLUDED

#include "Bird.hpp"


class CFlock;
class CPredator;


//! CPrey class
//! \ingroup Simulation
class CPrey : public CBird
{
  CPrey() = delete;
  CPrey(const CPrey&) = delete;
  CPrey& operator=(const CPrey&) = delete;
  CPrey(CPrey&&) = delete;
  CPrey& operator=(CPrey&&) = delete;

public:
  CPrey(int id, const glm::vec3& position, const glm::vec3& forward);
  virtual ~CPrey();

  virtual bool isPrey() const { return true; }
  virtual void NumPreyChanged();
  virtual void NumPredChanged();

  const Param::Prey& GetPreyParams() const { return pPrey_; }     //!< get prey parameter
  Param::Prey& GetPreyParams() { return pPrey_; }                 //!< get prey parameter
  void SetPreyParams(const Param::Prey& prey);                    //!< Set Prey parameter

  void update(float dt);

  float get_average_lat_acceleration() const { return average_lat_acceleration_; }
  float get_max_lat_acceleration() const { return max_lat_acceleration_; }
  float get_average_for_acceleration() const { return average_for_acceleration_; }
  float get_max_for_acceleration() const { return max_for_acceleration_; }
  float get_average_roll_rate() const { return average_roll_rate_; }
  float get_max_roll_rate() const { return max_roll_rate_; }
  long get_counter_acc() const { return counter_acc_; }
  void set_counter_acc(long counter) { counter_acc_ = counter; }
  void handleTrajectoryStorage();
  void handleManeuvers();

  typedef std::vector<glm::vec4>   Storage;
  Storage positionsAndSpeed;
  Storage externalPos;

private:
  friend struct find_neighbors_qf;

  //! Calculate flight forces
  // models of FlightDynamicsFn
  void flight_dynamic_artificial_prey(float dt);
  void flight_dynamic_stationary_prey(float dt);
  void flight_dynamic_external(float dt);

  void maneuver_smooth();
  void maneuver_optimal();
  void maneuver_non_smooth();
  void maneuver_non_smooth_adapt();
  void calculateAccelerations();

  //! Set closestPredator_ to closest predator if any in detect range.
  void testSettings();

public:
  const CPredator* associatedPredator_;

private:
  Param::Prey pPrey_;

public:
  float average_lat_acceleration_ = 0.0f;
  float minDist_pred_ = 999.0f;
  float average_out_of_bounds_ = 0.0f;
  float max_lat_acceleration_ = 0.0f;
  float average_for_acceleration_ = 0.0f;
  float max_for_acceleration_ = 0.0f;
  float average_roll_rate_ = 0.0f;
  float max_roll_rate_ = 0.0f;
  long counter_acc_ = 0;

private:
  // type of the flight dynamics member function
  typedef void (CPrey::*FlightDynamicFn)(float dt);
  // the according member function pointer
  FlightDynamicFn flight_dynamic_mem_fn_;
};


#endif
