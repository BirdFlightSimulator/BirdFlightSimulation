#ifndef GLSLINSTANCINGPROG_HPP_INCLUDED
#define GLSLINSTANCINGPROG_HPP_INCLUDED

#include <memory>
#include <iterator>
#include "mapped_buffer.hpp"
#include "glmfwd.hpp"


class GLSLStaticInstancingProg
{
  struct static_attrib_t
  {
    glm::vec4 forward_scale;
    glm::vec4 up;
    glm::vec4 position;
  };

public:
  GLSLStaticInstancingProg(unsigned ModelId, unsigned MaxN);
  virtual ~GLSLStaticInstancingProg();

  template <typename IT>
  void Instance(IT first, IT last, int ignoreId)
  {
    ssbo_.bind(GL_SHADER_STORAGE_BUFFER);
    ssbo_.map(sizeof(static_attrib_t) * MaxN_);
    instances_ = static_cast<unsigned>(std::min(MaxN_, static_cast<size_t>(std::distance(first, last))));
    static_attrib_t* dst = (static_attrib_t*)ssbo_.get();
    const float scale = ((*first)->id() == ignoreId) ? 0 : (*first)->wingSpan() * model_->modelScale();
	//std::cout << "model scale here " << model_->modelScale();
    for (unsigned i = 0; i < instances_; ++i, ++first, ++dst)
    {
      dst->forward_scale = glm::vec4((*first)->forward(), scale);
      dst->up = glm::vec4((*first)->up(), 0.f);
      dst->position = glm::vec4((*first)->position(), 1.0f);
    }
  }

  void Flush();
  void Render();

private:
  mutable unsigned instances_;
  mapped_buffer ssbo_;
  std::unique_ptr<class GLSLModel>  model_;
  glsl::program* pprog_;
  size_t MaxN_;
};


class GLSLBodyWingInstancingProg
{
  struct wing_attrib_t
  {
    float scale;
    float whatever;
	float beatCycle;
	float RS;
  };

public:
  GLSLBodyWingInstancingProg(unsigned BodyModelId, unsigned WingModelId, unsigned MaxN);
  ~GLSLBodyWingInstancingProg();

  template <typename IT>
  void Instance(IT first, IT last, int ignoreId)
  {
    body_.Instance(first, last, ignoreId);
    ssbo_.bind(GL_SHADER_STORAGE_BUFFER);
    ssbo_.map(sizeof(wing_attrib_t) * MaxN_);
    instances_ = static_cast<unsigned>(std::min(MaxN_, static_cast<size_t>(std::distance(first, last))));
    wing_attrib_t* dst = (wing_attrib_t*)ssbo_.get();
	//std::cout << "model scale here " << model_->modelScale();
    for (unsigned i = 0; i < instances_; ++i, ++first, ++dst)
    {
      dst->whatever = 10.0f;
	  dst->scale = (*first)->wingSpan() * model_->modelScale();
	  dst->beatCycle = (*first)->beatCycle_;
	  dst->RS = (*first)->RS_;
	  //std::cout << "\n" << dst->whatever;
    }

  }

  void Flush();
  void Render();

private:
  mutable unsigned instances_;
  mapped_buffer ssbo_;
  std::unique_ptr<class GLSLModel>  model_;
  glsl::program* pprog_;
  size_t MaxN_;
  GLSLStaticInstancingProg body_;
};


#endif
