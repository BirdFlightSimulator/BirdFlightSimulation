//! \defgroup Model The starling model implementation
//! \file Bird.hpp The 'bird'
//! \ingroup Model

#ifndef BIRD_HPP_INCLUDED
#define BIRD_HPP_INCLUDED

#include <algorithm>
#include "trail_buffer.hpp"
#include "Params.hpp"



//! bird class
//! \ingroup Simulation
class CBird
{
public:
  CBird(const CBird&) = delete;
  CBird& operator=(const CBird&) = delete;
  CBird(int id, const glm::vec3& position, const glm::vec3& forward);
  virtual ~CBird();

  virtual bool isPrey() const { return false; }
  virtual bool isPredator() const { return false; }
  virtual void NumPreyChanged() = 0;
  virtual void NumPredChanged() = 0;
  void RoostChanged();
  
  int id() const { return id_; }                                      //!< Returns unique id
  const Param::Bird& GetBirdParams() const { return pBird_; }         //!< Returns bird parameter
  Param::Bird& GetBirdParams() { return pBird_; }                     //!< Returns bird parameter
  void SetBirdParams(const Param::Bird& pBird);

  const glm::mat3& B() const {return B_; }                            //!< Body system
  const glm::mat3& H() const {return H_; }                            //!< Head system
  const glm::vec3& position() const { return position_; }             //!< position
  const glm::vec3& forward() const { return B_[0]; }                  //!< forward direction
  const glm::vec3& up() const { return B_[1]; }                       //!< up direction
  const glm::vec3& side() const { return B_[2]; }                     //!< side direction
  const glm::vec3& velocity() const { return velocity_; }             //!< velocity
  float speed() const { return speed_; }                              //!< speed
  void SetSpeed(float x);                                             //!< write speed
  void SetVelocity(glm::vec3 const& x);                               //!< write velocity

  float wingSpan() const { return wingSpan_; }

  //! \return Current reaction time
  float reactionTime() const { return reactionTime_; }

  //! \return Current reaction interval
  float reactionInterval() const { return reactionInterval_; }

  bool hasTrail() const { return (nullptr != trail_); }
  void setTrail(bool show);
  //! \return Mean reaction time
  float ReactionTime() const { return reactionTime_; }

protected:
  void induceControlError();
  void flight_dynamic(float dt);
  void integration(float dt);
  void regenerateLocalSpace(float dt);
  void nextReactionTime();

public:
  glm::vec3 position_;
  float roll_rate_ = 0.0f;
  float wingSpan_;
  glm::mat3 B_;          
  glm::mat3 H_; 
  glm::vec3 liftMax_;
  glm::vec3 velocity_;
  glm::vec3 accel_;
  float angular_acc_ = 0.0f;
  glm::vec3 force_;
  glm::vec3 gyro_;
  glm::vec3 steering_;
  glm::vec3 random_orientation_;
  float random_interval_ = 0.0f;
  Param::Bird pBird_;

protected:
  glm::vec3 lift_;
  glm::vec3 flightForce_;

 
  float   rand_ = 0.0f;
  float   desiredLift_ = 0.0f;
  float   prevDesiredLift_ = 0.0f;
  float   controlError_ = 0.0f;
  float   roll_error_ = 0.0f;
  float   speed_ = 0.0f;
  float   turn_ = 0.0f;
 
public:
  float   latACC_ = 0.0f;
  float   desLatACC_ = 0.0f;
  float   forACC_ = 0.0f;
  float   beatCycle_ = 0.0f;
  float   RS_ = 0;

protected:  
  bool		glide_ = true;
  float		span_ = 0.0f;
  int     keyState_;
  float   reactionTime_;
  float   reactionInterval_;
  float   StoreTrajectory_;
  int     separation_neighbors_;
  int     alignment_neighbors_;
  int     cohesion_neighbors_;
  int     interaction_neighbors_;

  trail_buffer* trail_;
  int           id_;
};


#endif
