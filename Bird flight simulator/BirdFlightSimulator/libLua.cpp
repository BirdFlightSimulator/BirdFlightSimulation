#include <exception>
#include "filesystem.hpp"
#include <luabind/adopt_policy.hpp>
#include <luabind/iterator_policy.hpp>
#include "Clock.hpp"
#include "libParam.hpp"
#include "libLua.hpp"
#include "libGlm.hpp"
#include "Globals.hpp"
#include "GLSLState.hpp"
#include "GLSLImm.hpp"
#include "GLWin.hpp"
#include "ICamera.hpp"
#include "IText.hpp"
#include "Camera.hpp"


extern void luaopen_libBirds(lua_State*);


namespace {


  void line(glm::dvec3 const& p0, glm::dvec3 const& p1, glm::dvec3 const& color)
  {
    GGl.imm3D->Begin(IMM_LINES);
    GGl.imm3D->Emit(glm::vec3(p0), color32(glm::vec3(color)));
    GGl.imm3D->Emit(glm::vec3(p1), color32(glm::vec3(color)));
    GGl.imm3D->End();
  }
}

namespace luabind { namespace detail {

  template <>
  static int iterator<Simulation::prey_collection::iterator>::next(lua_State* L)
  {
    iterator* self = static_cast<iterator*>(lua_touserdata(L, lua_upvalueindex(1)));
    if (self->first != self->last)
    {
      convert_to_lua(L, static_cast<CPrey*>(self->first->get()));
      ++self->first;
    }
    else
    {
      lua_pushnil(L);
    }
    return 1;
  }


  template <>
  static int iterator<Simulation::pred_collection::iterator>::next(lua_State* L)
  {
    iterator* self = static_cast<iterator*>(lua_touserdata(L, lua_upvalueindex(1)));
    if (self->first != self->last)
    {
      convert_to_lua(L, static_cast<CPredator*>(self->first->get()));
      ++self->first;
    }
    else
    {
      lua_pushnil(L);
    }
    return 1;
  }


}}


namespace liblua {
  
  using namespace luabind;
	using namespace Param;
  using namespace libParam;

  void SetInitialParameter(const object& obj) 
  { 
    SIM.SetInitialParameter(FromLua<Params>(obj)); 
  }

  void GetExperimentSettings(const object& obj)
  {
	  SIM.GetExperimentSettings(obj);
  }
    
  double SimulationTime() 
  { 
    return SIM.SimulationTime(); 
  }
    
  double RealTime() 
  { 
    return GlobalTimerNow(); 
  }
    
  GLWin& Window() 
  { 
    return APPWIN; 
  }
    
  ICamera* CreateCamera() 
  { 
    return new CCamera(); 
  }
    
  void SetActiveCamera(const object& luaobj)
  {
    SIM.SetActiveCamera(luaobj);
  }

  object GetActiveCamera()
  {
    return SIM.GetActiveCamera();
  }

  void ShowAnnotation(const char* txt, double duration) 
  { 
    GGl.setAnnotation(txt, duration); 
  }
    
  object GetRoost() 
  { 
    return ToLua<Roost>(Lua, PROOST); 
  }

      
  void SetRoost(const object& luaobj) 
  { 
    SIM.SetPRoost( FromLua<Roost>(luaobj) ); 
  }
    

  object GetRenderFlags()
  {
    return ToLua<Param::RenderFlags>(Lua, PRENDERFLAGS);
  }


  void SetRenderFlags(const object& luaobj)
  {
    SIM.SetPRenderFlags(FromLua<Param::RenderFlags>(luaobj));
  }


  void RegisterFactories(const object& PreyFactory, const object& PredatorFactory)
  {
    SIM.RegisterFactories(PreyFactory, PredatorFactory);
  }


  void RegisterDataStorage(const object& dataStorage)
  {
	  SIM.RegisterDataStorage(dataStorage);
  }

  void RegisterEvolution(const object& evolution_next)
  {
	  SIM.RegisterEvolution(evolution_next);
  }

  CPrey* NewPrey(int id, const glm::vec3& position, const glm::vec3& forward)
  {
    CPrey* ret = new CPrey(id, position, forward);
    return ret;
  }

  CPredator* NewPredator(int id, const glm::vec3& position, const glm::vec3& forward)
  {
    CPredator* ret = new CPredator(id, position, forward);

	//Robin insert, may be wrong
	//SIM.flock_->insert_pred(ret);
    return ret;
  }

  IText* Text() 
  { 
    return GTEXT.get(); 
  }

	CBird* GetBirdAtScreenPosition(int x, int y)
	{
		const glm::vec3 CSD = glm::vec3(GCAMERA.screenDirection(x, y));
		return SIM.PickNearestBird2Ray(glm::vec3(GCAMERA.eye()), CSD);
	}

  void QuitSimulation()
  {
    APPWIN.PostMessage(WM_CLOSE);
  }

  void DoEmulateKeyDown(unsigned KS)
  {
    APPWIN.PostMessage(WM_USER+0, WPARAM(KS));
  }

  unsigned SetNumPrey(unsigned newNum)
  {
    SIM.setNumPrey(newNum);
    return static_cast<unsigned>(SIM.prey().size());
  }

  unsigned SetNumPredators(unsigned newNum)
  {
    SIM.setNumPredators(newNum);
    return static_cast<unsigned>(SIM.pred().size());
  }

  double IntegrationTimeStep()
  {
    return PARAMS.IntegrationTimeStep;
  }

  double GetGeneration()
  {
	  return SIM.Generation_;
  }


  int PreyIterator(lua_State* L)
  {
    return luabind::detail::make_range(L, SIM.prey().begin(), SIM.prey().end());
  }


  int PredIterator(lua_State* L)
  {
    return luabind::detail::make_range(L, SIM.pred().begin(), SIM.pred().end());
  }


  int TrajectoryIterator(lua_State* L, const CPredator* pred)
  {
	  return luabind::detail::make_range(L, pred->hunts().Trajectory_.begin(), pred->hunts().Trajectory_.end());
  }

  
  int NumPrey()
  {
    return static_cast<int>(SIM.prey().size());
  }

  
  int NumPred()
  {
    return static_cast<int>(SIM.pred().size());
  }


  int nextID()
  {
    return SIM.nextID();
  }


  void open_libLua()
  {
    module(Lua, "Simulation")[
      def("SetInitialParameter", &SetInitialParameter),
		  def("GetExperimentSettings", &GetExperimentSettings),
      def("Window", &Window),
      def("CreateCamera", &CreateCamera, adopt(result)),
      def("GetActiveCamera", &GetActiveCamera),
      def("SetActiveCamera", &SetActiveCamera),
      def("SimulationTime", &SimulationTime),
      def("IntegrationTimeStep", &IntegrationTimeStep),
      def("RealTime", &RealTime),
      def("ShowAnnotation", &ShowAnnotation),
      def("GetRoost", &GetRoost),
      def("SetRoost", &SetRoost),
	    def("Generation", &GetGeneration),
      def("NewPrey", &NewPrey),
      def("NewPredator", &NewPredator),
      def("RegisterFactories", &RegisterFactories),
	    def("RegisterDataStorage", &RegisterDataStorage),
	    def("RegisterEvolution", &RegisterEvolution),
      def("Text", &Text),
			def("GetBirdAtScreenPosition", &GetBirdAtScreenPosition),
      def("GetRenderFlags", &GetRenderFlags),
      def("SetRenderFlags", &SetRenderFlags),
      def("Quit", &QuitSimulation),
      def("DoEmulateKeyDown", &DoEmulateKeyDown),
      def("SetNumPrey", &SetNumPrey),
      def("SetNumPredators", &SetNumPredators),
      def("Prey", &PreyIterator),
      def("Predators", &PredIterator),
      def("line", &line),
	    def("getTrajectory", &TrajectoryIterator),
      def("num_prey", &NumPrey),
      def("num_prdators", &NumPred),
      def("nextID", &nextID)
    ];
  }


  void Open(const char* ConfigFile)
  {
    char buffer[MAX_PATH];
    GetModuleFileName(NULL, buffer, MAX_PATH);
    auto ExePath = filesystem::path(buffer).parent_path();

    Lua.Open();
    luaopen_libBirds(Lua);
    open_libLua();
    LUA.cacheLuaSim();
    Lua.DoFile((ExePath / "/../../lua/BFS.lua").string().c_str());
	  LUASIM["Initialize"](ExePath.string().c_str(), ConfigFile);
  }

}
