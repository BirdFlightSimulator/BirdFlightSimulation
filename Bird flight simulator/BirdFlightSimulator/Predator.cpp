#include <cassert>
#include <glmutils/clip_length.hpp>
#include <glmutils/ray.hpp>
#include <glmutils/random.hpp>
#include <glmutils/save_normalize.hpp>
#include "random.hpp"
#include "glmfwd.hpp"
#include "Predator.hpp"
#include "Prey.hpp"
#include "Params.hpp"
#include "Globals.hpp"
#include "Mmsystem.h"
#include <stdlib.h> 
#include <iostream>
#include "ICamera.hpp"
#include <glm/gtc/matrix_access.hpp>


using namespace Param;

// this is illegal (M_PI is in the posix-standard)
//  # define M_PI           3.14159265358979323846
const float pi = 3.14159265358979323846f;



CPredator::hunt& CPredator::hunt::operator+=(const CPredator::hunt& h)
{
  sequences += h.sequences;
  locks += h.locks;
  success += h.success;
  if (minDist < h.minDist) velocityMinDist = h.velocityMinDist;
  minDist = std::min(minDist, h.minDist);
  lastMinDist = h.minDist;
  minDistLockedOn = std::min(minDistLockedOn, h.minDistLockedOn);
  seqTime += h.seqTime;
  lookTime += h.lookTime;
  std::copy(h.attackSize.begin(), h.attackSize.end(), std::back_inserter(attackSize));
  std::copy(h.catchSize.begin(), h.catchSize.end(), std::back_inserter(catchSize));
  return *this;
}


CPredator::CPredator(int ID, const glm::vec3& position, const glm::vec3& forward)
	: CBird(ID, position, forward),
	handleTime_(0),
	dogFight_(0),
	lockedOn_(0),
	closestPrey_(0),
	targetPrey_(0),
	targetPoint_(0),
	locks_(0)
{
}


CPredator::~CPredator()
{
}


void CPredator::SetPredParams(const Param::Predator& pPred)
{
  pPred_ = pPred;
}


void CPredator::NumPreyChanged()
{
  lockedOn_ = 0;
  closestPrey_ = 0;
  targetPrey_ = 0;
}


void CPredator::NumPredChanged()
{
}


void CPredator::update(float dt)
{
	if (pPred_.StoreTrajectory && (StoreTrajectory_ += dt) >= SIM.Params().evolution.Trajectories.dt)
	{
		handle_trajectory_storage(true);
		StoreTrajectory_ = 0.0f;
	}
  const CPrey* target = GetTargetPrey();
  if ((reactionTime_ += dt) >= reactionInterval_)
  {
    steering_ = glm::vec3(0);
	handle_direct_attack();
	hunts_.seqTime += reactionTime_;
  }

  // Physics works in real time...
  if (SIM.Params().evolution.artificialPredator)
  {
	  flightDynamic_artificial_predator();
  }
  else
  {
	  flight_dynamic(dt);
  }
  regenerateLocalSpace(dt);

  if (reactionTime_ >= reactionInterval_)
  {
  	induceControlError(); 
    // calculate time of next reaction
    nextReactionTime();
  }



  if (target)
  {
    // Check for collisions in real-time
	  const float distance = glm::distance(position_, target->position());
	  if (is_attacking_) checkEndHunt(target->position());
	  if (distance < hunts_.minDist && is_attacking_)
	  {
		  hunts_.minDist = distance;
		  hunts_.velocityMinDist = glm::length(velocity_);
		  hunts_.InterceptionState = handle_trajectory_storage(false);
		  if (hunts_.minDist < 0.2)
		  {
			  if (GCAMERA.GetFocalBird()->id() == id_) SIM.catch_ = 1;
			  EndHunt(false);
		  }
	  }
	  if (distance < hunts_.lastMinDist)
	  {
		  hunts_.lastMinDist = distance;
	  }
  }
  float yo = id();
  if (id() == 4) yo -= rand() / float(RAND_MAX) + 0.2;
  appendTrail(trail_, position_, B_[2], yo / 5.0f, dt, RS_, pBird_.wingSpan*4.0f,beatCycle_, B_[1]);
}


void CPredator::Accelerate()
{
	//! There is no acceleration or deceleration in the model currently; it always accelerates as much as possible
	steering_ += 0.0f * B_[0];
}


Param::Trajectory CPredator::handle_trajectory_storage(bool storeInVector)
{
	Param::Trajectory traj;
	const CPrey* target = GetTargetPrey();

	traj.Pred_acc = accel_;
	traj.Pred_forward = forward();
	traj.Pred_gen = pBird_.generation;
	traj.Pred_id = id_;
	traj.Pred_position = position_;
	traj.Pred_up = up();
	traj.N = pPred_.N;
	traj.Pred_velocity = velocity_;
	traj.Prey_acc = target->accel_;
	traj.Prey_forward = target->forward();
	traj.Prey_id = target->id();
	traj.Prey_position = target->position_;
	traj.Prey_up = target->up();
	traj.Prey_velocity = target->velocity_;
	traj.Prey_forACC = target->forACC_;
	traj.time = SIM.SimulationTime();
	traj.Pred_roll_acc = angular_acc_;
	traj.Pred_roll_rate = roll_rate_;
	traj.Prey_roll_acc = target->angular_acc_;
	traj.Prey_roll_rate = target->roll_rate_;
	traj.Pred_desLatACC = desLatACC_;
	traj.Pred_latACC = latACC_;
	traj.Prey_latACC = target->latACC_;

	if (storeInVector) hunts_.Trajectory_.push_back(traj);
	return(traj);
	
}

//! This is a hacky way of controlling predator. Should be improved
void CPredator::handle_user_controls()
{

	if (!GetAsyncKeyState(VK_F11) && keyState_ != 0)
	{
		keyState_ = 0;
	}
	if (GetAsyncKeyState(VK_F11) && keyState_ == 0)
	{
		testSettings();

	}

	if (GetAsyncKeyState(VK_DOWN)) steering_ += 5.0f*glm::vec3(0, 1, 0);
	if (GetAsyncKeyState(VK_NUMPAD2)) steering_ += glm::length(liftMax_)*glm::vec3(0, 1, 0);
	if (GetAsyncKeyState(VK_UP)) steering_ -= 5.0f*glm::vec3(0, 1, 0);;
	if (GetAsyncKeyState(VK_NUMPAD8)) steering_ -= glm::length(liftMax_)*glm::vec3(0, 1, 0);
	if (GetAsyncKeyState(VK_RIGHT)) steering_ += 5.0f*H_[2];
	if (GetAsyncKeyState(VK_NUMPAD6)) steering_ += glm::length(liftMax_)*H_[2];
	if (GetAsyncKeyState(VK_LEFT)) steering_ -= 5.0f*H_[2];
	if (GetAsyncKeyState(VK_NUMPAD4)) steering_ -= glm::length(liftMax_)*H_[2];
	if (GetAsyncKeyState(VK_NUMPAD5)) steering_ += 3.0f*H_[0];
	if (GetAsyncKeyState(VK_NUMPAD0)) steering_ += 1.0f*H_[0];

	if (GetAsyncKeyState(VK_F7))
	{
		position_.y = 0.0f;
		position_.x = 600.0f;
		position_.z = 20.0f;

		glm::vec3 test = glmutils::save_normalize(-position_, glm::vec3(0));

		B_[0] = glmutils::save_normalize(-position_, glm::vec3(1, 0, 0));
		if (abs(B_[0].y - 1) > 0.001)
		{
			B_[2] = glm::normalize(glm::cross(B_[0], glm::vec3(0, 1, 0)));
			glm::cross(B_[2], B_[0]);
		}
		velocity_ = 20.0f *B_[0];
		pPred_.N = 2.0f;
	}
	if (GetAsyncKeyState(VK_NUMPAD5)) pPred_.N = 5.0f;

}



void CPredator::flightDynamic_artificial_predator()
{
	speed_ = pPred_.artificial.x;
	lift_ = std::min(desiredLift_, pPred_.artificial.y * pBird_.bodyMass)*B_[1];
	liftMax_ = pPred_.artificial.y * pBird_.bodyMass * B_[1];
	forACC_ = 0.0;
	latACC_ = pPred_.artificial.y;
	flightForce_ = lift_;
	flightForce_.y -= pBird_.bodyMass * 9.81f;
	angular_acc_ = pPred_.artificial.z;

	velocity_ = glmutils::save_normalize(velocity_, velocity_) * speed_;
}


void CPredator::handle_direct_attack()
{
	const CPrey* target = GetTargetPrey();
    assert(target);
	if (!is_attacking_) return;
	glm::vec3 targetPosition = target->position();
	glm::vec3 targetVelocity = target->velocity_;
	glm::vec3 targetAcceleration = target->accel_;
	if (pPred_.PursuitStrategy == 1) proportionalNavigation(targetPosition, targetVelocity);
	if (pPred_.PursuitStrategy == 2) DirectPursuit(targetPosition, targetVelocity);
	if (pPred_.PursuitStrategy == 3) DirectPursuit2(targetPosition, targetVelocity);
	if (pPred_.PursuitStrategy == 4) PNDP(targetPosition, targetVelocity);
	if (pPred_.PursuitStrategy == 5) proportionalNavigation_error_estimation_velocity(targetPosition, targetVelocity);
	if (pPred_.PursuitStrategy == 6) proportional_navigation_kalman_filtered(targetPosition, targetVelocity);
	if (pPred_.PursuitStrategy == 7) augProportionalNavigation(targetPosition, targetVelocity, targetAcceleration);
}



void CPredator::BeginHunt()
{
  ++hunts_.sequences;
  handleTime_ = 0;
  hunts_.lastMinDist = 999.99f;
  is_attacking_ = true;
}


void CPredator::EndHunt(bool success)
{
	
  if (success) ++hunts_.success;
  lockedOn_ = 0;
  closestPrey_ = 0;
  dogFight_ = 0;
  locks_ = 0;
  handleTime_ = 0.0f;
  is_attacking_ = false;
}


void CPredator::ResetHunt()
{
  EndHunt(false);
  hunts_ = hunt();
}


void CPredator::SetTargetPrey(const CPrey* prey)
{
  targetPrey_ = prey;
}


void CPredator::DirectPursuit(const glm::vec3& targetPosition, const glm::vec3& targetVelocity)
{

	glm::vec3 r = targetPosition - position_;
	glm::vec3 rp = r;

	rp += pPred_.DPAdjParam * targetVelocity; 
	glm::vec3 v = velocity_;
	glm::vec3 omega = glm::cross(v, rp) / glm::length(glm::cross(v, rp));
	float angle = asin(glm::length(glm::cross(v, rp)) / (glm::length(v)*glm::length(rp)));
	omega *= angle;
	omega *= glm::length(v);
	omega = glm::cross(omega, v);
	steering_ += omega * pPred_.N;
}

void CPredator::checkEndHunt(const glm::vec3& targetPosition)
{
	glm::vec3 r = targetPosition - position_;

	if (hunts_.minDist< 5)
	{
		glm::vec3 pHead = glm::vec3(glm::dot(r, glm::column(H_, 0)), glm::dot(r, glm::column(H_, 1)), glm::dot(r, glm::column(H_, 2)));
		float phi = atan2(pHead.z, pHead.x);
		bool blind = (abs(phi) - 3.14 < 0.8 && pHead.x < 0);
		if (glm::length(r) > 10) {
			EndHunt(false);
			SIM.catch_ = 2;
		}
		if (blind) {
			EndHunt(false);
			SIM.catch_ = 2;
		}
	}


}

void CPredator::DirectPursuit2(const glm::vec3& targetPosition, const glm::vec3& targetVelocity)
{

	glm::vec3 r = targetPosition - position_;
	glm::vec3 rp = r;
	rp += pPred_.DPAdjParam * targetVelocity;
	steering_ += glmutils::save_normalize(rp, glm::vec3(0)) * pPred_.N * 100.0f;  
}


void CPredator::PNDP(const glm::vec3& targetPosition, const glm::vec3& targetVelocity)
{
	glm::vec3 r = targetPosition - position_;
	if (glm::length(r) > 300) DirectPursuit2(targetPosition, targetVelocity);
	else proportionalNavigation(targetPosition, targetVelocity);
}


void CPredator::proportionalNavigation(const glm::vec3& targetPosition, const glm::vec3& targetVelocity)
{
	glm::vec3 r = targetPosition - position_;
	glm::vec3 v = velocity_ - targetVelocity;
	glm::vec3 wLOS = glm::cross(v, r) / glm::dot(r, r);
	steering_ += pPred_.N * glm::cross(wLOS, velocity_)*pBird_.bodyMass;

}


void CPredator::augProportionalNavigation(const glm::vec3& targetPosition, const glm::vec3& targetVelocity, const glm::vec3& targetAcceleration)
{
	glm::vec3 r = targetPosition - position_;
	glm::vec3 v = velocity_ - targetVelocity;
	glm::vec3 LOS = r / glm::length(r);
	glm::vec3 tA = glm::cross(LOS,glm::cross(targetAcceleration, LOS));
	glm::vec3 wLOS = glm::cross(v, r) / glm::dot(r, r);
	steering_ += pPred_.N * (glm::cross(wLOS, velocity_)*pBird_.bodyMass + 0.5f*0.5f*tA);

}


//! Prop Nav function used in Mills (2018)
void CPredator::proportionalNavigation_error_estimation_velocity(const glm::vec3& targetPosition, const glm::vec3& targetVelocity)
{	

	glm::vec3 r = targetPosition - position_;
  float LOS = std::sqrt(glm::dot(r, r));
	r = addVisualError(targetPosition - position_);
	glm::vec3 r_adj = pPred_.N2 * r + (1 - pPred_.N2) * position_;
	glm::vec3 relative_velocity = (r_adj - previous_relative_position_) / reactionInterval_ * -1.0f;	
	glm::vec3 v = relative_velocity;
	glm::vec3 wLOS = glm::cross(v, r) / glm::dot(r, r);
	steering_ += pPred_.N * glm::cross(wLOS, velocity_)*pBird_.bodyMass;
	previous_relative_position_ = r_adj;
}


void CPredator::proportional_navigation_kalman_filtered(const glm::vec3& targetPosition, const glm::vec3& targetVelocity)
{
	//kalman filter
	pPred_.filter = id_;
	r_real_ = targetPosition - position_;
	v_real_ = targetPrey_->velocity_ - velocity_;
	a_real_ = targetPrey_->accel_ - accel_;
	observe(targetPosition - position_);
	if (pPred_.filter != 3) predict();
	if (pPred_.filter == 3) predict_extended_kalman_filter();
	update();

	if (pPred_.filter == 0)
	{
		v_est_ = v_obs_;
		r_est_ = r_obs_;
		a_t_est_ = a_t_obs_;
	}
	if (pPred_.filter == 1)
	{
		
		r_est_ = targetPosition - position_;
		v_est_ = (r_est_ - r_prev_) / reactionInterval_;
		a_t_est_ = (v_obs_ - v_prev_) / reactionInterval_ + steering_prev_ / pBird_.bodyMass;
	}
	if (pPred_.filter == 2)
	{
		//r_est_ = glm::vec3(1.0f,0.0f,0.0f);
	}

	//guidance
	glm::vec3 v = -v_est_;
	glm::vec3 r = r_est_;
	glm::vec3 wLOS = glm::cross(v, r) / glm::dot(r, r);
	steering_ += pPred_.N * glm::cross(wLOS, velocity_)*pBird_.bodyMass;
	steering_prev_ = steering_;
	r_prev_ = r_est_;
	v_prev_ = v_est_;
	a_t_prev_ = a_t_est_;
	P_prev_ = P_;
}

glm::vec3 CPredator::addVisualError(const glm::vec3& relative_position)
{
	float theta = std::uniform_real_distribution<float>()(rnd_eng()) * 2.0f * pi;
	float r = std::uniform_real_distribution<float>()(rnd_eng());
	glm::vec2 error_magnitude = glm::vec2(r * sin(theta),r * cos(theta)) * pPred_.VisualError;
	glm::vec3 z(0, 1, 0);
	glm::vec3 unit_pos = glmutils::save_normalize(relative_position , glm::vec3(1, 0, 0));
	glm::vec3 axis1 = glmutils::save_normalize(glm::cross(z, unit_pos),glm::vec3(1, 0, 0));
	glm::vec3 axis2 = glm::cross(axis1, unit_pos);
	glm::vec3 rd = relative_position + error_magnitude * glm::transpose(glm::mat2x3(axis1, axis2)) * glm::length(relative_position);
	return rd;
}


void CPredator::observe(const glm::vec3& relative_position)
{
	glm::vec3 z(0, 1, 0);
	glm::vec3 unit_pos = glmutils::save_normalize(relative_position, glm::vec3(1, 0, 0));
	glm::vec3 axis1 = glmutils::save_normalize(glm::cross(z, unit_pos), glm::vec3(1, 0, 0));
	glm::vec3 axis2 = glm::cross(axis1, unit_pos);
	glm::mat3 eigenvectors = glm::mat3(unit_pos, axis1, axis2);
	float err = glm::length(relative_position)*pPred_.VisualError;
	glm::mat3 eigenvalues = glm::mat3(err*0.1f,0,0,0,err ,0, 0,0, err);
	glm::vec3 noise = glm::vec3(std::normal_distribution<float>(0, 1)(rnd_eng()), std::normal_distribution<float>(0, 1)(rnd_eng()), std::normal_distribution<float>(0, 1)(rnd_eng()));
	noise = eigenvectors * eigenvalues * noise;
	obs_cov_ = glm::inverse(eigenvectors) * eigenvalues * eigenvectors;
	untracked_cov_ = glm::mat3(0.1f,0,0, 0, 0.1f, 0, 0, 0, 0.1f) ;
	r_obs_ = relative_position + noise;
	v_obs_ = (r_obs_ - r_prev_) / reactionInterval_;
	a_t_obs_ = (v_obs_ - v_prev_) / reactionInterval_ + steering_prev_ / pBird_.bodyMass;

	if (glm::length(r_prev_) < 0.01)
	{
		r_prev_ = r_obs_;
		v_prev_ = targetPrey_->velocity_ - velocity_;
		a_t_prev_ = targetPrey_->accel_;
		v_obs_ = v_prev_;
		a_t_obs_ = a_t_prev_;
	}

	return;
}


void CPredator::predict(void)
{
	glm::mat3 F = glm::transpose(glm::mat3(1.0f, reactionInterval_, 0.5*reactionInterval_*reactionInterval_,
		0.0f, 1.0f, reactionInterval_,
		0.0f, 0.0f, 1.0f));
	glm::mat3 x = glm::transpose(glm::mat3(r_prev_, v_prev_, a_t_prev_*0.0f));
	glm::vec3 B = glm::vec3(-0.5f*reactionInterval_*reactionInterval_, reactionInterval_, 0.0f);
	glm::mat3 x_new = glm::transpose(F*x + glm::outerProduct(B, steering_prev_/pBird_.bodyMass));
	P_ = F*P_prev_*glm::transpose(F) + untracked_cov_;
	r_est_ = x_new[0];
	v_est_ = x_new[1];
	a_t_est_ = x_new[2];
	return;
}


void CPredator::predict_extended_kalman_filter(void)
{
	float dt = SIM.Params().IntegrationTimeStep;
	glm::mat3 F = glm::transpose(glm::mat3(1.0f, dt, 0.5*dt*dt,
		0.0f, 1.0f, dt,
		0.0f, 0.0f, 1.0f));
	glm::mat3 x = glm::transpose(glm::mat3(r_prev_, v_prev_, a_t_prev_*0.0f));
	glm::vec3 B = glm::vec3(-0.5f*dt*dt, dt, 0.0f);

	float steps = reactionInterval_ / dt;
	glm::mat3 x_new = x;
	for (int n; n < steps; n++)
	{
		 x_new = F*x_new + glm::outerProduct(B, steering_prev_ / pBird_.bodyMass);
	}
	x_new = glm::transpose(x_new);
	P_ = F*P_prev_*glm::transpose(F) + untracked_cov_;
	r_est_ = x_new[0];
	v_est_ = x_new[1];
	a_t_est_ = x_new[2];
	return;
}

void CPredator::update(void)
{
	glm::vec3 x = glm::vec3(r_est_);
	glm::vec3 z = glm::vec3(r_obs_);

	glm::mat3 H = glm::mat3();
	//Kalman gain
	glm::mat3 K = P_*glm::transpose(H) * glm::inverse(H*P_*glm::transpose(H) + obs_cov_);
    //update
	r_est_ = x + K*(z - H*x);
	P_ = P_ - K*H*P_;
	v_est_ = (0.2f*v_est_ + 0.8f*(r_est_ - r_prev_) / reactionInterval_);
	a_t_est_ = 0.5f*(a_t_est_ + ((v_est_ - v_prev_) / reactionInterval_ + steering_prev_ / pBird_.bodyMass));

	
}





void CPredator::testSettings()
{
	keyState_ = 1;
	SIM.PrintFloat(float(GetTargetPrey()->id() ), "targetID");
	SIM.PrintFloat(pBird_.wingMass, "wing mass");
	SIM.PrintFloat(float(pPred_.PursuitStrategy), "pursuit strategy");
	SIM.PrintFloat(float(pBird_.generation), "Generation");
	SIM.PrintFloat(pBird_.InertiaBody, "InertiaBody");
	SIM.PrintFloat(pBird_.J, "J");
	SIM.PrintFloat(pBird_.bodyMass, "bodymass");
	SIM.PrintFloat(pBird_.bodyArea, "bodyArea");
	SIM.PrintFloat(pBird_.cBody, "cBody");
	SIM.PrintFloat(pBird_.cFriction, "cFriction");
	SIM.PrintFloat(pBird_.contMaxLift, "contMaxLift");
	SIM.PrintFloat(pBird_.cruiseSpeed, "cruiseSpeed");
	SIM.PrintFloat(pBird_.wingSpan, "wingSpan");
	SIM.PrintFloat(pBird_.wingBeatFreq, "wingBeatFreq");
	SIM.PrintFloat(pBird_.bodyWeight, "bodyWeight");
	SIM.PrintFloat(pBird_.rho, "rho");
	SIM.PrintFloat(pBird_.InertiaWing, "Inertia wing");
	SIM.PrintFloat(pBird_.speedControl, "speedControl");
	SIM.PrintFloat(pPred_.VisualError, "VisualError");
	SIM.PrintVector(B_[0], "body x");
	SIM.PrintVector(B_[1], "body y");
	SIM.PrintVector(B_[2], "body z");
	SIM.PrintFloat(latACC_, "actual lateral acceleration");
	SIM.PrintVector(addVisualError(glm::vec3(0, 0, 1)), "test visual error");
}