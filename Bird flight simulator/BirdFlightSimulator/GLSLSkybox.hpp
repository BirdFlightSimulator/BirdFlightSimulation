#ifndef SLSKYBOX_HPP_INCLUDED
#define SLSKYBOX_HPP_INCLUDED

#include "glmfwd.hpp"
#include <glsl/texture.hpp>
#include <glsl/buffer.hpp>
#include <glsl/vertexarray.hpp>


class GLSLSkybox
{
public:
  GLSLSkybox();
  virtual ~GLSLSkybox();
  void Flush();
  void Render();

private:
  glsl::vertexarray vao_;       // dummy one
  glsl::texture     CubeMap_;
};


#endif  // GLSLSKYBOX_HPP_INCLUDED
