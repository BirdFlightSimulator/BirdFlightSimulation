#ifndef GLSL_EXCEPTION_HPP_INCLUDED
#define GLSL_EXCEPTION_HPP_INCLUDED


#include <exception>
#include <string>


namespace glsl {

  class exception : public std::exception
  {
  public:
    exception(const char* msg) : std::exception(msg) {}
    exception(const std::string& msg) : std::exception(msg.c_str()) {}
    ~exception() {};
  };

}

#endif

