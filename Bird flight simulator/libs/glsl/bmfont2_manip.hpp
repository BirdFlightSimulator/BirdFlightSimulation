#ifndef BMFONT2_MANIP_HPP_INCLUDED
#define BMFONT2_MANIP_HPP_INCLUDED

#include <stdint.h>


namespace glsl{ namespace bmfont2 {

  namespace manip {

    struct font
    {
      explicit font(std::string const& fontAppName) : fontAppName(fontAppName) {}
      std::string fontAppName;
    };


    struct color
    {
      explicit color(uint32_t Val) : val(val) {}
      color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255) : val(r << 24 | g << 16 | b << 8 | a) {}
      color(float r, float g, float b, float a = 1.0f) : val(uint32_t(255 * r) << 24 | uint32_t(255 * g) << 16 | uint32_t(255 * b) << 8 | uint32_t(255 * a)) {}
      uint32_t val;
    };


    struct tab
    {
      explicit tab(int tabSize) : val(tabSize) {}
      int val;
    };


    struct line_height
    {
      explicit line_height(int lineHeight) : val(lineHeight) {}
      int val;
    };


    struct cursor
    {
      cursor(float X, float Y) : x(X), y(Y) {}
      float x, y;
    };


    struct orig
    {
      orig(float X, float Y, float Z) : x(X), y(Y), z(Z) {}
      float x, y, z;
    };

  }


} }

#endif

