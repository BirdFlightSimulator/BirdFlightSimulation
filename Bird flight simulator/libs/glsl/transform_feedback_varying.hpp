//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file varying.hpp Thin wrapper for GLSL varyings.

#ifndef GLSL_VARYING_HPP_INCLUDED
#define GLSL_VARYING_HPP_INCLUDED

#include "glsl.hpp"
#include "proxy.hpp"


namespace glsl {

  class transform_feedback_varying
  {
  public:
    transform_feedback_varying(GLuint p, const char* name);
    transform_feedback_varying(GLuint p, GLuint index);

    operator GLuint () const { return index_; }
    GLuint  index() const { return index_; }
    GLsizei  size() const { return size_; }
    GLenum  type() const { return type_; }
    const char* name() const { return name_.c_str(); }

  private:
    GLuint      index_;
    GLsizei     size_;
    GLenum      type_;
    std::string name_;
  };

}  // namespace glsl


#endif



