//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file gpu_memory_info.hpp Support for NVX_gpu_memory_info

#ifndef GLSL_GPU_MEMORY_INFO_HPP_INCLUDED
#define GLSL_GPU_MEMORY_INFO_HPP_INCLUDED

#include "glsl.hpp"


namespace glsl {


  //! Dedicated video memory, total size (in kb) of the GPU memory.
  //! Returns 0 if NVX_gpu_memory_info is not supported.
  GLint dedicated_vidmem_nvx();

  //! total available memory, total size (in Kb) of the memory available for allocations.
  //! Returns 0 if NVX_gpu_memory_info is not supported.
  GLint total_available_memory_nvx();

  //! current available dedicated video memory (in kb), currently unused GPU memory.
  //! Returns -1 if NVX_gpu_memory_info is not supported.
  GLint current_available_vidmem_nvx();

  //! count of total evictions seen by system.
  //! Returns -1 if NVX_gpu_memory_info is not supported.
  GLint eviction_count_nvx();

  //! size of total video memory evicted (in kb).
  //! Returns -1 if NVX_gpu_memory_info is not supported.
  GLint evicted_memory_nvx();

}


#endif
