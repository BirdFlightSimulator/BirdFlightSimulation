#include <string>
#include <algorithm>
#include <stb/stb_image.h>
#include "bmfont2.hpp"
#include "bmfont2_io.hpp"


namespace glsl { namespace bmfont2 {


  struct CharDescr
  {
    CharDescr() {}
    explicit CharDescr(wchar_t Id): id(Id), xadvance(0) {}
    CharDescr(const struct FileChar& chr, int YOfs)
    :  id((wchar_t)chr.id),
      xadvance(chr.xadvance),
      TexCoord0(chr.x, chr.y, chr.page),
      TexCoord1(chr.x + chr.width, chr.y + chr.height, chr.page),
      Vert0(chr.xoffset, chr.yoffset - YOfs),
      Vert1(chr.xoffset + chr.width, chr.yoffset + (chr.height -YOfs))
    {}

    wchar_t id;
    float xadvance;
    glm::u8vec3 TexCoord0, TexCoord1;      // [left-top, right-bottom] [tu, tv, page-index]
    glm::vec2   Vert0, Vert1;              // [left-top, right-bottom] [x, y]
  };


  bool operator < (const CharDescr& a, const CharDescr b) 
  { 
    return a.id < b.id; 
  }


  struct KerningPair
  {
    KerningPair() {}
    KerningPair(const struct FileKerningPair& kp)
      : first((wchar_t)kp.first), second((wchar_t)kp.second), amount(kp.amount)
    {
    }

    KerningPair(int First, int Second)
      : first(First), second(Second), amount(0)
    {
    }

    wchar_t first, second;
    int16_t amount;
  };


  bool operator < (const KerningPair& a, const KerningPair& b)
  {
    return (a.first < b.first) ? (a.second < b.second) : false;
  }

  
  struct FontImpl
  {
    FontImpl::FontImpl() 
    {
      FontName = "Unkonwn";
      FontAppName = "Unknown";
      FontSize = 0;
      BitField = 0;
      LineHeight = 0;
      Base = 0;
    }


    FontImpl::FontImpl(const char* FontAppName, const char* BMFontDescr)
    {
      BMFontLoader FL;
      try {
        FL.Parse(BMFontDescr);
      }
      catch (const char* e) {
        std::string msg = (std::string("glsl::FontImpl: Error reading \"") + BMFontDescr + "\"" + e);
        glDebugMessageInsert(
          GL_DEBUG_SOURCE_THIRD_PARTY,
          GL_DEBUG_TYPE_ERROR, 0xCECE,
          GL_DEBUG_SEVERITY_HIGH, -1,
          msg.c_str());
        return;
      }
      FontName = FL.pFontName;
      this->FontAppName = FontAppName;
      FontSize = std::abs(FL.pInfoBlk->fontSize);
      BitField = FL.pInfoBlk->bitField;
      LineHeight = FL.pCommonBlk->lineHeight;
      Base = (int)FL.pCommonBlk->base;
      TexSize = glm::ivec2(FL.pCommonBlk->scaleW, FL.pCommonBlk->scaleH);  
      int CharCount = FL.CharCount;
      CharMap.reserve(CharCount);
      for (int chr=0; chr<CharCount; ++chr)
      {
        CharMap.emplace_back(FL.pCharBlk[chr], Base);
      }
      int KerningCount = FL.KerningCount;
      KerningMap.reserve(KerningCount);
      for (int page=0; page<FL.KerningCount; ++page)
      {
        KerningMap.emplace_back(FL.pKerningBlk[page]);
      }
      PagePath = std::move(FL.PagePath);
    }


    float xadvance(wchar_t current, wchar_t next) const
    {
      float advance = 0.0f;
      {
        auto it = std::lower_bound(CharMap.begin(), CharMap.end(), CharDescr(current));
        if ((it != CharMap.end()) && (it->id == current)) advance = it->xadvance;
      }
      {
        auto it = std::lower_bound(KerningMap.begin(), KerningMap.end(), KerningPair(current, next));
        if ((it != KerningMap.end()) && (it->first == current) && (it->second == next)) advance += it->amount;
      }
      return advance;
    }


    CharDescr const* FontImpl::map(wchar_t chr) const
    {
      auto it = std::lower_bound(CharMap.cbegin(), CharMap.cend(), CharDescr(chr));
      if ((it != CharMap.end()) && (it->id == chr))
      {
        return &*it;
      }
      return nullptr;
    }

    std::vector<CharDescr> CharMap;
    std::vector<KerningPair> KerningMap;
    std::string FontName;
    std::string FontAppName;
    int FontSize;
    int BitField;
    int LineHeight;
    int Base;
    glm::ivec2 TexSize;
    std::vector<std::string> PagePath;
  };


  void FontLoader::AddFont(const char* FontAppName, const char* BMFontDescr)
  {
    std::shared_ptr<FontImpl> pimpl( new FontImpl(FontAppName, BMFontDescr) );
    fonts_.push_back(pimpl);
  }
  

  FontArray::FontArray(FontLoader&& Loader) : fonts_(std::move(Loader.fonts_))
  {
    // Adjust pages and create texture array
    size_t pages = 0;
    texSize_ = glm::ivec2(0);
    std::vector<std::string> PagePath;
    for (auto f : fonts_) {
      FontImpl* pimpl = f.get();
      auto & CharMap = pimpl->CharMap;
      for (auto & chrdscr : CharMap) {
        chrdscr.TexCoord0.z += static_cast<uint8_t>(pages);
        chrdscr.TexCoord1.z += static_cast<uint8_t>(pages);
      }
      texSize_ = glm::max(texSize_, f->TexSize);
      pages += f->PagePath.size();
      std::copy(f->PagePath.begin(), f->PagePath.end(), std::back_inserter(PagePath));
    }

    // Load and compile OpenGL array texture
    glsl::texture tex(GL_TEXTURE_2D_ARRAY);
    tex.bind();
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_R8, texSize_.x, texSize_.y, static_cast<GLsizei>(pages));
    tex.set_wrap_filter(GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR);
    for (int page = 0; page < pages; ++page)
    {
      int w, h, c;
      stbi_uc* texData = stbi_load(PagePath[page].c_str(), &w, &h, &c, STBI_grey);
      if (texData)
      {
        glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, page, w, h, 1, GL_RED, GL_UNSIGNED_BYTE, (void*)texData);
        stbi_image_free(texData);
      }
      else
      {
        std::string msg = (std::string("Unable to decode texture file: ") + PagePath[page] + stbi_failure_reason()).c_str();
        glDebugMessageInsert(
          GL_DEBUG_SOURCE_THIRD_PARTY,
          GL_DEBUG_TYPE_ERROR, 0xCECE,
          GL_DEBUG_SEVERITY_HIGH, -1,
          msg.c_str());
      }
    }
    tex.unbind();
    tex_.swap(tex);
  }


  std::shared_ptr<FontArray> FontArray::Create(FontLoader&& Loader)
  {
    return std::shared_ptr<FontArray>( new FontArray(std::move(Loader)) );
  }


  Font FontArray::GetFont(const char* FontAppName)
  {
    auto it = std::find_if(fonts_.cbegin(), fonts_.cend(),
      [=](std::shared_ptr<struct FontImpl> const& a) {
        return 0 == std::strcmp(a->FontAppName.c_str(), FontAppName);
      }
    );
    if (it == fonts_.end()) {
      std::string msg = (std::string(FontAppName) + " not in FontArray").c_str(); 
      glDebugMessageInsert(
        GL_DEBUG_SOURCE_THIRD_PARTY,
        GL_DEBUG_TYPE_ERROR, 0xCECE,
        GL_DEBUG_SEVERITY_HIGH, -1,
        msg.c_str());
      return Font();
    }
    return Font(*it);
  }

  
  void FontArray::BindTexture(unsigned int Unit) const
  {
    tex_.bind(Unit);
  }


  Font::Font() : pimpl_(new FontImpl())
  {
  }


  Font::Font(std::shared_ptr<struct FontImpl> pimpl)
    : pimpl_(pimpl)
  {
  }

  
  const char* Font::FontName() const { return pimpl_->FontName.c_str(); }
  const char* Font::FontAppName() const { return pimpl_->FontAppName.c_str(); }
  int Font::FontSize() const { return pimpl_->FontSize; }
  int Font::LineHeight() const { return pimpl_->LineHeight; }
  int Font::Base() const { return pimpl_->Base; }
  bool Font::is_unicode() const { return 1 == (pimpl_->BitField & (1 << 1)); }
  bool Font::is_italic() const { return 1 == (pimpl_->BitField & (1 << 2)); }
  bool Font::is_bold() const { return 1 == (pimpl_->BitField & (1 << 3)); }

  

  GlyphBufStream::GlyphBufStream() : font_()
  {
  }


  GlyphBufStream::GlyphBufStream(std::shared_ptr<FontArray> fontArray) 
  : cursor_(0), 
    orig_(0),
    tabSize_(100),
    lineHeight_(0),
    color_(0xFFFFFFFF),
    pArray_(fontArray)
  {
  }


  Font const& GlyphBufStream::GetFont() const
  {
    return font_;
  }


  void GlyphBufStream::Clear()
  {
    attribs_.clear();
    cursor_.x = orig_.x;
    cursor_.y = orig_.y + font_.LineHeight();
  }


  void GlyphBufStream::streamPair(wchar_t chr, wchar_t next)
  {
    if (chr == '\n')
    {
      cursor_.x = orig_.x; 
      cursor_.y += lineHeight_;
    }
    else if (chr == '\t')
    {
      float tabs = cursor_.x / tabSize_;
      cursor_.x = (tabs + 1) * tabSize_;
    }
    else
    {
      CharDescr const* pdscr = font_.pimpl_->map(chr);
      if (pdscr)
      {
        VUS3_CLIPUS1_CUB4_TUB3 a0 = { glm::u16vec3(cursor_ + glm::vec2(pdscr->Vert0), orig_.z), 0, color_, pdscr->TexCoord0 };
        VUS3_CLIPUS1_CUB4_TUB3 a1 = { glm::u16vec3(cursor_ + glm::vec2(pdscr->Vert1), orig_.z), 0, color_, pdscr->TexCoord1 };
        attribs_.push_back(a0);
        attribs_.push_back(a1);
        cursor_.x += font_.pimpl_->xadvance(chr, next);
      }
    }
  }


  void GlyphBufStream::move(glyph_range const& range, float dx, float dy)
  {
    adjustRange(range, glm::vec2(dx, dy));
  }


  void GlyphBufStream::leftFlush(glyph_range const& range, float x)
  {
    auto box = rangeBox(range);
    move(range, x - box.p0().x, 0.0f);
  }

  void GlyphBufStream::rightFlush(glyph_range const& range, float x)
  {
    auto box = rangeBox(range);
    auto ext = glmutils::extent(box);
    move(range, (x - ext.x) - box.p0().x, 0.0f);
  }


  void GlyphBufStream::hCenter(glyph_range const& range, float x, float width)
  {
    auto ac = x + width / 2.0f;
    auto box = rangeBox(range);
    auto ext = glmutils::extent(box);
    move(range, (ac - ext.x / 2.0f) - box.p0().x, 0.0f);
  }


  void GlyphBufStream::vCenter(glyph_range const& range, float y, float height)
  {
    auto ac = y + height / 2.0f;
    auto box = rangeBox(range);
    auto ext = glmutils::extent(box);
    move(range, 0.0f, (ac - ext.y / 2.0f) - box.p0().y);
  }


  void GlyphBufStream::center(glyph_range const& range, glm::vec4 const& a)
  {
    auto acx = a.x + a.z / 2.0f;
    auto acy = a.y + a.w / 2.0f;
    auto box = rangeBox(range);
    auto ext = glmutils::extent(box);
    move(range, (acx - ext.x / 2.0f) - box.p0().x, (acy - ext.y / 2.0f) - box.p0().y);
  }


  glmutils::bbox2 GlyphBufStream::rangeBox(glyph_range const& range) const
  {
    if (range.first == 0) return glmutils::bbox2(cursor_, cursor_);
    auto pAttribs = AttribPointer();
    size_t last = std::min(range.second, VertexCount());
    glmutils::bbox2 box(glm::vec2(pAttribs[range.first].vertex));
    for (size_t p = range.first; p < last; ++p) {
      glmutils::include(box, glm::vec2(pAttribs[p].vertex));
    }
    return box;
  }


  void GlyphBufStream::adjustRange(glyph_range const& range, glm::vec2 const& delta)
  {
    auto pAttribs = const_cast<VUS3_CLIPUS1_CUB4_TUB3*>(AttribPointer());
    size_t last = std::min(range.second, VertexCount());
    glm::u16vec3 delta3(delta, 0);
    for (size_t p = range.first; p < last; ++p) {
      pAttribs[p].vertex += delta3;
    }
  }


  GlyphBufStream& GlyphBufStream::operator << (char chr)
  {
    return this->operator<<(wchar_t(chr));
  }


  GlyphBufStream& GlyphBufStream::operator << (wchar_t chr)
  {
    streamPair(chr, ' ');
    return *this;
  }


  GlyphBufStream& GlyphBufStream::operator << (const char* str)
  {
    if (str == nullptr || *str == '\0') return *this;
    do {
      streamPair(wchar_t(*str), wchar_t(*(str + 1)));
      ++str;
    } while(*str != '\0');
    return *this;
  }

  
  GlyphBufStream& GlyphBufStream::operator << (const wchar_t* str)
  {
    if (str == nullptr || *str == '\0') return *this;
    do {
      streamPair(*str, *(str + 1));
      ++str;
    } while (*str != '\0');
    return *this;
  }


  GlyphBufStream& GlyphBufStream::operator << (std::string const& str)
  {
    if (str.empty()) return *this;
    return this->operator<<(str.c_str());
  }


  GlyphBufStream& GlyphBufStream::operator << (std::wstring const& str)
  {
    if (str.empty()) return *this;
    return this->operator<<(str.c_str());
  }


  GlyphBufStream& GlyphBufStream::operator << (const manip::font& x)
  {
    if (pArray_) {
      font_ = pArray_->GetFont(x.fontAppName.c_str());
      lineHeight_ = font_.LineHeight();
    }
    return *this;
  }


  GlyphBufStream& GlyphBufStream::operator << (const manip::cursor& x)
  { 
    cursor_ = glm::vec2(x.x, x.y);
    return *this;
  }

  
  GlyphBufStream& GlyphBufStream::operator << (const manip::orig& x) 
  { 
    orig_ = glm::vec3(x.x, x.y, x.z);
    cursor_.x = orig_.x;
    cursor_.y = orig_.y + font_.LineHeight();
    return *this; 
  }


  GlyphBufStream& GlyphBufStream::operator << (const manip::color& x) 
  { 
    color_ = x.val; 
    return *this; 
  }
  
  
  GlyphBufStream& GlyphBufStream::operator << (const manip::tab& x) 
  { 
    tabSize_ = x.val; 
    return *this; 
  }

  
  GlyphBufStream& GlyphBufStream::operator << (const manip::line_height& x) 
  { 
    lineHeight_ = x.val; return *this; 
  }

} }

