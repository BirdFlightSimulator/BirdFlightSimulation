#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "wgl.h"
#include "wgl_context.hpp"
#include "tchar.h"


namespace glsl {


  namespace {

    
    class glslDummyWin
    {
    public:
      glslDummyWin() : hInstance(NULL), hWnd(NULL), cAtom(NULL), hRC(NULL)  
      {
        GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, nullptr, &hInstance);
        WNDCLASS wc;
        memset((void*)&wc, 0, sizeof(WNDCLASS));
        wc.style = CS_OWNDC;
        wc.lpfnWndProc = &WndProc;
        wc.hInstance = hInstance;
        wc.lpszClassName = _T("GLSLDummyWindowClass");
        cAtom = RegisterClass(&wc);
        hWnd = CreateWindow( 
          _T("GLSLDummyWindowClass"),        // name of window class 
          _T("GLSLDummyWindow"),         
          WS_OVERLAPPED, 
          CW_USEDEFAULT,
          CW_USEDEFAULT,
          CW_USEDEFAULT,
          CW_USEDEFAULT,
          (HWND) NULL,
          (HMENU) NULL,
          hInstance, 
          (LPVOID)(this));    
      }


      void Destroy() 
      {
        HDC hDC = GetDC(hWnd);
        wglMakeCurrent(hDC, NULL);
        wglDeleteContext(hRC);
        DestroyWindow(hWnd);
        UnregisterClass(_T("GLSLDummyWindowClass"), hInstance);
        FreeLibrary(hInstance);
      }


      static LRESULT WINAPI WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
      {
        switch (uMsg)
        {
          case WM_CREATE:
          {
            glslDummyWin* self = (glslDummyWin*)((LPCREATESTRUCT)(lParam))->lpCreateParams;
            HDC hDC = GetDC(hwnd);
            PIXELFORMATDESCRIPTOR pfd;
            memset((void*)&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
            pfd.nSize  = sizeof(PIXELFORMATDESCRIPTOR);
            pfd.nVersion   = 1;
            pfd.dwFlags    = PFD_SUPPORT_OPENGL;
            pfd.iPixelType = PFD_TYPE_RGBA;
            int nPixelFormat = ChoosePixelFormat(hDC, &pfd);
            if (nPixelFormat == 0) return NULL;
            if (FALSE == SetPixelFormat (hDC, nPixelFormat, &pfd)) return NULL; 
            self->hRC = wglCreateContext(hDC);
            wglMakeCurrent(hDC, self->hRC);
            return 0;
          }
        }
        return DefWindowProc(hwnd, uMsg, wParam, lParam); 
      }

    public:
      ATOM    cAtom;
      HWND    hWnd;
      HGLRC   hRC;
      HMODULE hInstance;
    };

  
    HGLRC DoCreateContext(HDC hDC, int* attributes, int colorSamples, int coverageSamples, int dblBuf)
    {
      glslDummyWin dWin;
      HGLRC hRC = NULL;
      BOOL Status = FALSE;
      if (wgl_LOAD_SUCCEEDED == wgl_LoadFunctions(hDC))
      {
        if (wgl_ext_ARB_pixel_format)
        {
          int wgl_sample_buffer_arb = 0;
          int wgl_samples_arb = 0;
          int wgl_coverage_samples = 0;
          if (wgl_ext_ARB_multisample && (colorSamples > 0))
          {
            wgl_sample_buffer_arb = WGL_SAMPLE_BUFFERS_ARB;
            if ((0 == coverageSamples) || !wgl_ext_NV_multisample_coverage)
            {
              wgl_samples_arb = WGL_SAMPLES_ARB;
            }
            else if (wgl_ext_NV_multisample_coverage)
            {
              wgl_samples_arb = WGL_COLOR_SAMPLES_NV;
              wgl_coverage_samples = WGL_COVERAGE_SAMPLES_NV;
            }
          }
          int iAttribs[2][24] = { 
          { 
            WGL_SUPPORT_OPENGL_ARB,   1,                          // Must support OGL rendering
            WGL_DRAW_TO_WINDOW_ARB,   1,                          // pf that can run a window
            WGL_ACCELERATION_ARB,     WGL_FULL_ACCELERATION_ARB,  // must be HW accelerated
            WGL_COLOR_BITS_ARB,       32,                         // 8 bits of each R, G and B
            WGL_DEPTH_BITS_ARB,       24,                         // 24 bits of depth precision for window
            WGL_PIXEL_TYPE_ARB,       WGL_TYPE_RGBA_ARB,          // pf should be RGBA type
            wgl_sample_buffer_arb,    (colorSamples ? 1 : 0),
            wgl_samples_arb,          colorSamples,
            wgl_coverage_samples,     coverageSamples,
            0, 0 
          },
          { 
            WGL_SUPPORT_OPENGL_ARB,   1,                          // Must support OGL rendering
            WGL_DRAW_TO_WINDOW_ARB,   1,                          // pf that can run a window
            WGL_ACCELERATION_ARB,     WGL_FULL_ACCELERATION_ARB,  // must be HW accelerated
            WGL_COLOR_BITS_ARB,       32,                         // 8 bits of each R, G and B
            WGL_DEPTH_BITS_ARB,       24,                         // 24 bits of depth precision for window
            WGL_PIXEL_TYPE_ARB,       WGL_TYPE_RGBA_ARB,          // pf should be RGBA type
            WGL_SWAP_METHOD_ARB,      WGL_SWAP_EXCHANGE_ARB,      // Exchange, don't copy
            WGL_DOUBLE_BUFFER_ARB,    1,                          // Double buffered context
            wgl_sample_buffer_arb,    (colorSamples ? 1 : 0),
            wgl_samples_arb,          colorSamples,
            wgl_coverage_samples,     coverageSamples,
            0, 0 
          }};
          FLOAT fAttribs[] = { 0, 0 };
          int pixelFormat;
          UINT numFormats;
          BOOL e = wglChoosePixelFormatARB(hDC, iAttribs[dblBuf], fAttribs, 1, &pixelFormat, &numFormats);
          if (numFormats)
          {
            if (SetPixelFormat(hDC, pixelFormat, NULL))
            {
              if (wgl_ext_ARB_create_context)
              {
                hRC = wglCreateContextAttribsARB(hDC, 0, attributes);
              }
            }
          }
        }
      }
      dWin.Destroy();
      wglMakeCurrent(hDC, hRC);
      return hRC;
    }
  
  }


  HGLRC CreateContext(HDC hDC, int* attributes, int colorSamples, int coverageSamples)
  {
    return DoCreateContext(hDC, attributes, colorSamples, coverageSamples, 1);
  }


  HGLRC CreateSingleBufContext(HDC hDC, int* attributes, int colorSamples, int coverageSamples)
  {
    return DoCreateContext(hDC, attributes, colorSamples, coverageSamples, 0);
  }

}

#endif
