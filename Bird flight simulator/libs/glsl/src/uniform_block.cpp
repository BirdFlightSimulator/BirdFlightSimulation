//
// GLSL support library
// Hanno Hildenbrandt 2008
//

#include <cassert>
#include <algorithm>
#include <string.h>
#include "uniform_block.hpp"


namespace glsl {


  uniform_block::uniform_block(GLuint p, GLuint index, GLint binding)
:  program_(p), index_(index)
{
  assert(GL_INVALID_INDEX != index_);
  char name[256] = { 0 };
  glGetActiveUniformBlockName(program_, index_, 255, 0, &name[0]);
  name_ = name;
  this->binding(binding);
}


uniform_block::uniform_block(GLuint p, const char* name, GLint binding)
:  program_(p), name_(name)
{
  assert(GL_INVALID_INDEX != index_);
  index_ = glGetUniformBlockIndex(p, name);
  char ubname[256] = { 0 };
  glGetActiveUniformBlockName(program_, index_, 255, 0, ubname);
  name_ = ubname;
  this->binding(binding);
}


GLint uniform_block::binding(GLint binding)
{
  glUniformBlockBinding(program_, index_, binding);
  return binding;
}


GLint uniform_block::binding() const
{
  GLint param;
  glGetActiveUniformBlockiv(program_, index_, GL_UNIFORM_BLOCK_BINDING, &param);
  return param;
}


GLint uniform_block::data_size() const
{
  GLint param;
  glGetActiveUniformBlockiv(program_, index_, GL_UNIFORM_BLOCK_DATA_SIZE, &param);
  return param;
}


}  // namespace glsl





