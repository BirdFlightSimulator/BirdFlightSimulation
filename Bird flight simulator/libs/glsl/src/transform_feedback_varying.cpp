//
// GLSL support library
// Hanno Hildenbrandt 2008
//

#include <algorithm>    // swap
#include "transform_feedback_varying.hpp"


namespace glsl {


  transform_feedback_varying::transform_feedback_varying(GLuint p, GLuint index)
  : index_(GL_INVALID_INDEX), size_(0), type_(GL_NONE)
  {
    GLchar vname[256] = { 0 };
    glGetTransformFeedbackVarying(p, index, 255, 0, &size_, &type_, vname);
    index_ = index;
    name_ = vname;
  }


  transform_feedback_varying::transform_feedback_varying(GLuint p, const char* name)
  : index_(GL_INVALID_INDEX), size_(0), type_(GL_NONE)
  {
    GLint n;
    glGetProgramiv(p, GL_TRANSFORM_FEEDBACK_VARYINGS, &n);
    for (GLint i=0; i<n; ++i)
    {
      transform_feedback_varying vi(p, (GLuint)i);
      if (vi.name_ == name)
      {
        std::swap(*this, vi);
        break;
      }
    }
  }


}  // namespace glsl





