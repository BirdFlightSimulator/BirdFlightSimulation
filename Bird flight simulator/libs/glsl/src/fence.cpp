/*
 * fence.cpp
 *
 *  Created on: Oct 29, 2009
 *      Author: hanno
 */

#include <cassert>
#include "fence.hpp"


namespace glsl {

  fence::fence()
  {
  }

  fence::~fence()
  {
  }

  GLsync fence::get() const
  {
    GLsync h = get_();
    if (0 == h) { h = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0); this->reset_(h); }
    return h;
  }

  GLenum fence::client_wait(GLuint64 timeout)
  {
    return glClientWaitSync(get_(), GL_SYNC_GPU_COMMANDS_COMPLETE, timeout);
  }

  void fence::wait()
  {
    glWaitSync(get_(), GL_SYNC_GPU_COMMANDS_COMPLETE, GL_TIMEOUT_IGNORED);
  }

  void fence::swap(fence& other)
  {
    swap_(other);
  }

}
