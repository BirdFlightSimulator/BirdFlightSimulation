//
// GLSL support library
// Hanno Hildenbrandt 2008
//

#include <algorithm>    // swap
#include "attribute.hpp"


namespace glsl {

  attribute::attribute(GLuint p, GLuint index, GLint location)
  : index_(GL_INVALID_INDEX), type_(GL_NONE), size_(0), location_(-1)
  {
    GLchar aname[256] = { 0 };
    glGetActiveAttrib(p, index, 255, 0, &size_, &type_, aname);
    if (0 <= location) glBindAttribLocation(p, location, aname);
    this->index_ = index;
    this->location_ = glGetAttribLocation(p, aname);
    this->name_ = aname;
  }

  attribute::attribute(GLuint p, const char* name, GLint location)
  : index_(GL_INVALID_INDEX), type_(GL_NONE), size_(0), location_(-1)
  {
    GLint n;
    glGetProgramiv(p, GL_ACTIVE_ATTRIBUTES, &n);
    for (GLint i=0; i<n; ++i)
    {
      attribute ai(p, i, -1);
      if (ai.name_ == name)
      {
        if (0 <= location) glBindAttribLocation(p, location, name);
        ai.location_ = glGetAttribLocation(p, name);
        std::swap(*this, ai);
        break;
      }
    }
  }


}  // namespace glsl
