//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file swap_control.hpp Vertical synchronization and swap control.
//! Supports \c GLX_EXT_swap_control and \c WGL_EXT_swap_control

#ifndef GLSL_SWAP_CONTROL_HPP_INCLUDED
#define GLSL_SWAP_CONTROL_HPP_INCLUDED

#include "glsl.hpp"


namespace glsl {

  //! Enable vertical synchronization, see <tt>GLX_EXT_swap_control, WGL_EXT_swap_cotrol</tt>
  void enable_vsync();

  //! Disable vertical synchronization, see <tt>GLX_EXT_swap_control, WGL_EXT_swap_cotrol</tt>
  void disable_vsync();

  //! Fine grained swap control, see <tt>GLX_EXT_swap_control, WGL_EXT_swap_cotrol</tt>
  void swap_control(int interval);
}


#endif


