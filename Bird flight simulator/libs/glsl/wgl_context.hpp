//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file wgl_context.hpp WGL context creation


#ifndef _WIN32 
  #error wgl not supported
#endif

#ifndef GLSL_WGL_CONTEXT_HPP_INCLUDED
#define GLSL_WGL_CONTEXT_HPP_INCLUDED


#define WIN32_MEAN_AND_LEAN
#include <windows.h>


namespace glsl {

  HGLRC CreateContext(HDC hDC, int* attributes, int colorSamples = 0, int coverageSamples = 0);
  HGLRC CreateSingleBufContext(HDC hDC, int* attributes, int colorSamples = 0, int coverageSamples = 0);

}


#endif
