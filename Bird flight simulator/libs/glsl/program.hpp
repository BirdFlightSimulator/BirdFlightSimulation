//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file program.hpp Thin Wrapper for GLSL Shader Program.

#ifndef GLSL_PROGRAM_HPP_INCLUDED
#define GLSL_PROGRAM_HPP_INCLUDED

#include "glsl.hpp"
#include <string>
#include <vector>
#include "shader.hpp"
#include "attribute.hpp"
#include "uniform.hpp"
#include "uniform_block.hpp"
#include "transform_feedback_varying.hpp"

/**
\class glsl::program program.hpp
Thin Wrapper for GLSL shader Program.
Stand alone usage example:
\code
glsl::program prog(vertexShaderSource, fragmentShaderSource, 0);
prog.link();  // link the program
prog.use();    // use the program
prog.uniform("MyUniform")->set(1.0f)
...Render...
prog.useGL();  // back to OpenGL fixed functionality
\endcode

\sa shader_pool
**/

namespace glsl {

  class program
  {
  public:
    //! Creates empty program (use attach)
    program();

    //! Creates program from source
    program(const char* vertexSource,
            const char* fragmentSource,
            const char* geometrySource);

    //! Creates program from existing \c shader
    program(shader const& vertexShader,
            shader const& fragmentShader,
            shader const& geomertyShader);

    //! Frees all resources
    ~program();

    operator GLuint () const { return get(); }  //!< Cast to underlying OpenGL object.
    GLuint get() const;                         //!< Returns the underlying OpenGL object.

    bool attach(shader const& sh);  //!< Attach the shader \c s.
    bool detach(GLenum type);       //!< Detach the shader of type \c type.
    bool link();                    //!< (Re)Link the \c program. Returns LINK_STATUS == TRUE

    glsl::program* use() const;     //!< Use the \c program

    bool validate() const;
    GLint status(GLenum pname) const;       //!< Returns the \c pname - status
    std::string log_info() const;           //!< Returns the OpenGL log info.

    shader get_shader(GLenum type) const;  //!< Return shader of type \c type

    void swap(program& other);  //!< Swap

    GLint geometry_input() const;      //!< Returns GEOMETRY_INPUT_TYPE or GL_NONE.
    GLint geometry_output() const;     //!< Returns GEOMETRY_OUTPUT_TYPE or GL_NONE.
    GLint geometry_vertices() const;   //!< Returns GEOMETRY_VERTICES_OUT or GL_NONE.

    GLuint active_attributes() const;                                     //!< Returns the number of active attributes
    glsl::attribute* attribute(const char* aname, GLint location = -1);   //!< Returns the attribute named /c aname

    GLuint active_uniforms() const;              //!< Returns the number of active uniforms
    glsl::uniform* uniform(const char* uname);   //!< Returns the uniform named /c uname

    GLuint active_uniform_blocks() const;                     //!< Returns the number of active uniform blocks
    glsl::uniform_block* uniform_block(const char* bname);    //!< Returns the uniform block named \c bname

    GLuint active_varyings() const;               //!< Returns the number of active varyings
    glsl::transform_feedback_varying* varying(const char* vname);    //!< Returns varying with name \c vname

    //! Specify the transform feedback varyings to record.
    //! s. \c glTransformFeedbackVaryings.
    void feedback_varyings(std::vector<std::string> const& varyings, bool interleaved);

  private:
    std::shared_ptr<struct sh_prog_state> state_;
  };


}  // namespace glsl


#endif



