//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file uniform.hpp Thin wrapper for GLSL Uniform Variables.

#ifndef GLSL_UNIFORM_HPP_INCLUDED
#define GLSL_UNIFORM_HPP_INCLUDED

#include "glsl.hpp"
#include "proxy.hpp"


namespace glsl {


  //! Thin Wrapper for GLSL Uniform Variables.
  class uniform
  {
  public:
    //! Constructs an unassigned uniform
    uniform();

    //! Constructs from program-name pair
    uniform(GLuint p, const char* name);

    //! Constructs from program-index pair
    uniform(GLuint p, GLint index);

    GLuint index() const { return index_; }             //!< Returns the uniform index.
    GLenum type() const { return type_; }               //!< Returns the uniform type.
    const char* name() const { return name_.c_str(); }  //!< Returns the uniform name.
    GLint size() const { return size_; }                //!< Returns the uniform size.
    operator GLint (void) const { return loc_; }        //!< Cast to OpenGL location
    GLint location() const { return loc_; }             //!< Returns the uniform location.

    //! \name Pass through glUniform
    //@{
    void set1i(GLint v0) const { glUniform1i(loc_, v0); }
    void set2i(GLint v0, GLint v1) const { glUniform2i(loc_, v0, v1); }
    void set3i(GLint v0, GLint v1, GLint v2) const { glUniform3i(loc_, v0, v1, v2); }
    void set4i(GLint v0, GLint v1, GLint v2, GLint v3) const { glUniform4i(loc_, v0, v1, v2, v3); }

    void set1iv(GLuint count, const GLint* v) const { glUniform1iv(loc_, count, v); }
    void set2iv(GLuint count, const GLint* v) const { glUniform2iv(loc_, count, v); }
    void set3iv(GLuint count, const GLint* v) const { glUniform3iv(loc_, count, v); }
    void set4iv(GLuint count, const GLint* v) const { glUniform4iv(loc_, count, v); }

    void set1ui(GLint v0) const { glUniform1ui(loc_, v0); }
    void set2ui(GLint v0, GLint v1) const { glUniform2ui(loc_, v0, v1); }
    void set3ui(GLint v0, GLint v1, GLint v2) const { glUniform3ui(loc_, v0, v1, v2); }
    void set4ui(GLint v0, GLint v1, GLint v2, GLint v3) const { glUniform4ui(loc_, v0, v1, v2, v3); }

    void set1uiv(GLuint count, const GLuint* v) const { glUniform1uiv(loc_, count, v); }
    void set2uiv(GLuint count, const GLuint* v) const { glUniform2uiv(loc_, count, v); }
    void set3uiv(GLuint count, const GLuint* v) const { glUniform3uiv(loc_, count, v); }
    void set4uiv(GLuint count, const GLuint* v) const { glUniform4uiv(loc_, count, v); }

    void set1f(GLfloat v0) const { glUniform1f(loc_, v0); }
    void set2f(GLfloat v0, GLfloat v1) const { glUniform2f(loc_, v0, v1); }
    void set3f(GLfloat v0, GLfloat v1, GLfloat v2) const { glUniform3f(loc_, v0, v1, v2); }
    void set4f(GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) const { glUniform4f(loc_, v0, v1, v2, v3); }

    void set1fv(GLuint count, const GLfloat* v) const { glUniform1fv(loc_, count, v); }
    void set2fv(GLuint count, const GLfloat* v) const { glUniform2fv(loc_, count, v); }
    void set3fv(GLuint count, const GLfloat* v) const { glUniform3fv(loc_, count, v); }
    void set4fv(GLuint count, const GLfloat* v) const { glUniform4fv(loc_, count, v); }

    void set2x2fv(GLuint count, const GLfloat* mat, bool transp = false) const { glUniformMatrix2fv(loc_, count, transp, mat); }
    void set3x3fv(GLuint count, const GLfloat* mat, bool transp = false) const { glUniformMatrix3fv(loc_, count, transp, mat); }
    void set4x4fv(GLuint count, const GLfloat* mat, bool transp = false) const { glUniformMatrix4fv(loc_, count, transp, mat); }
    void set2x3fv(GLuint count, const GLfloat* mat, bool transp = false) const { glUniformMatrix2x3fv(loc_, count, transp, mat); }
    void set3x2fv(GLuint count, const GLfloat* mat, bool transp = false) const { glUniformMatrix3x2fv(loc_, count, transp, mat); }
    void set2x4fv(GLuint count, const GLfloat* mat, bool transp = false) const { glUniformMatrix2x4fv(loc_, count, transp, mat); }
    void set3x4fv(GLuint count, const GLfloat* mat, bool transp = false) const { glUniformMatrix3x4fv(loc_, count, transp, mat); }
    void set4x2fv(GLuint count, const GLfloat* mat, bool transp = false) const { glUniformMatrix4x2fv(loc_, count, transp, mat); }
    void set4x3fv(GLuint count, const GLfloat* mat, bool transp = false) const { glUniformMatrix4x3fv(loc_, count, transp, mat); }
    //@}

    //! \name Overloads for base types
    //@{
    void set(bool v0) const { set1i(v0); }
    void set(GLint v0) const { set1i(v0); }
    void set(GLuint v0) const { set1ui(v0); }
    void set(GLfloat v0) const { set1f(v0); }

    void set(bool v0, bool v1) const { set2i(v0, v1); }
    void set(GLint v0, GLint v1) const { set2i(v0, v1); }
    void set(GLuint v0, GLuint v1) const { set2ui(v0, v1); }
    void set(GLfloat v0, GLfloat v1) const { set2f(v0, v1); }

    void set(bool v0, bool v1, bool v2) const { set3i(v0, v1, v2); }
    void set(GLint v0, GLint v1, GLint v2) const { set3i(v0, v1, v2); }
    void set(GLuint v0, GLuint v1, GLuint v2) const { set3ui(v0, v1, v2); }
    void set(GLfloat v0, GLfloat v1, GLfloat v2) const { set3f(v0, v1, v2); }

    void set(bool v0, bool v1, bool v2, bool v3) const { set4i(v0, v1, v2, v3); }
    void set(GLint v0, GLint v1, GLint v2, GLint v3) const { set4i(v0, v1, v2, v3); }
    void set(GLuint v0, GLuint v1, GLuint v2, GLuint v3) const { set4ui(v0, v1, v2, v3); }
    void set(GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) const { set4f(v0, v1, v2, v3); }

    void set(GLuint n, const GLint* v) const { set1iv(n, v); }
    void set(GLuint n, const GLuint* v) const { set1uiv(n, v); }
    void set(GLuint n, const GLfloat* v) const { set1fv(n, v); }
    //@}

#ifdef GLM_VERSION
    //! \name Overloads for glm types
    //! If you include \c <glm/glm.h> before <tt>uniform.hpp</tt> you can use
    //! the the following overloads:
    //@{
    void set(const glm::bvec2& v) const { set2i(v.x, v.y); }
    void set(const glm::ivec2& v) const { set2i(v.x, v.y); }
    void set(const glm::vec2& v) const { set2f(v.x, v.y); }

    void set(const glm::bvec3& v) const { set3i(v.x, v.y, v.z); }
    void set(const glm::ivec3& v) const { set3i(v.x, v.y, v.z); }
    void set(const glm::vec3& v) const { set3f(v.x, v.y, v.z); }

    void set(const glm::bvec4& v) const { set4i(v.x, v.y, v.z, v.w); }
    void set(const glm::ivec4& v) const { set4i(v.x, v.y, v.z, v.w); }
    void set(const glm::vec4& v) const { set4f(v.x, v.y, v.z, v.w); }

    void set(const glm::mat2& mat) const { glUniformMatrix2fv(loc_, 1, false, (float*)&mat); }
    void set(const glm::mat3& mat) const { glUniformMatrix3fv(loc_, 1, false, (float*)&mat); }
    void set(const glm::mat4& mat) const { glUniformMatrix4fv(loc_, 1, false, (float*)&mat); }
    void set(const glm::mat2x3& mat) const { glUniformMatrix2x3fv(loc_, 1, false, (float*)&mat); }
    void set(const glm::mat3x2& mat) const { glUniformMatrix3x2fv(loc_, 1, false, (float*)&mat); }
    void set(const glm::mat4x3& mat) const { glUniformMatrix4x3fv(loc_, 1, false, (float*)&mat); }
    void set(const glm::mat4x2& mat) const { glUniformMatrix4x2fv(loc_, 1, false, (float*)&mat); }
    void set(const glm::mat3x4& mat) const { glUniformMatrix3x4fv(loc_, 1, false, (float*)&mat); }
    void set(const glm::mat2x4& mat) const { glUniformMatrix2x4fv(loc_, 1, false, (float*)&mat); }
    //@}
#endif

  private:
    GLuint      index_;
    GLenum      type_;
    std::string name_;
    GLint       size_;
    GLint       loc_;
  };


}  // namespace glsl


#endif



