#ifndef BMFFONT2_HPP_INCLUDED
#define BMFFONT2_HPP_INCLUDED


#include <exception>
#include <memory>
#include <cstdint>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_precision.hpp>
#include <glmutils/bbox.hpp>
#include "texture.hpp"
#include "bmfont2_manip.hpp"


namespace glsl { namespace bmfont2 {


#pragma pack(push, 1)
  struct VUS3_CLIPUS1_CUB4_TUB3
  {
    glm::u16vec3  vertex;
    uint16_t      clipIdx;
    uint32_t      color;
    glm::u8vec3   tex;
  };
#pragma pack(pop)


  class FontLoader
  {
  public:
    void AddFont(const char* FontAppName, const char* BMFontDescr);

  private:
    friend class FontArray;
    std::vector<std::shared_ptr<struct FontImpl>> fonts_;
  };


  class Font
  {
    Font();
    explicit Font(std::shared_ptr<struct FontImpl> pimpl);

  public:
    const char* FontName() const;
    const char* FontAppName() const;
    int FontSize() const;
    int LineHeight() const;
    int Base() const;
    bool is_unicode() const;
    bool is_italic() const;
    bool is_bold() const;

  private:
    friend class FontArray;
    friend class GlyphBufStream;
    std::shared_ptr<struct FontImpl> pimpl_;
  };


  class FontArray
  {
    FontArray(FontLoader&& Loader);

  public:
    static std::shared_ptr<FontArray> Create(FontLoader&& Loader);

    Font GetFont(const char* FontAppName);
    void BindTexture(unsigned int Unit) const;
    glm::ivec2 TextureSize() const { return texSize_; }

  private:
    glsl::texture tex_;
    glm::ivec2 texSize_;
    std::vector<std::shared_ptr<struct FontImpl>> fonts_;
  };


  class GlyphBufStream
  {
  public:
    typedef std::vector<VUS3_CLIPUS1_CUB4_TUB3> vertex_buffer;
    typedef std::pair<size_t, size_t> glyph_range;
    
    GlyphBufStream();
    GlyphBufStream(std::shared_ptr<FontArray>);

    Font const& GetFont() const;
    glm::vec2 GetCursor() const { return cursor_; }
    glm::vec3 GetOrig() const { return orig_; }

    // Alignment
    glyph_range beginRange() const { return glyph_range(VertexCount(), 0); }
    glyph_range endRange(glyph_range const& range) const { return glyph_range(range.first, VertexCount()); }
    glmutils::bbox2 rangeBox(glyph_range const&) const;

    void move(glyph_range const& range, float dx, float dy);
    void leftFlush(glyph_range const& range, float x);
    void rightFlush(glyph_range const& range, float x);
    void hCenter(glyph_range const& range, float x, float width);
    void vCenter(glyph_range const& range, float y, float height);
    void center(glyph_range const& range, glm::vec4 const& a);
    
    // Buffer related
    void Clear();
    size_t VertexCount() const { return attribs_.size(); }
    size_t PrimCount() const { return attribs_.size() >> 1; }
    VUS3_CLIPUS1_CUB4_TUB3 const* AttribPointer() const { return attribs_.empty() ? nullptr : &attribs_[0]; }
    vertex_buffer const& GetBuffer() const { return attribs_; }

    // Streaming
    GlyphBufStream& operator << (char chr);
    GlyphBufStream& operator << (wchar_t chr);
    GlyphBufStream& operator << (const char* str);
    GlyphBufStream& operator << (const wchar_t* str);
    GlyphBufStream& operator << (std::string const& str);
    GlyphBufStream& operator << (std::wstring const& str);

    // Manipulators
    GlyphBufStream& operator << (const manip::font& x);
    GlyphBufStream& operator << (const manip::color& x);
    GlyphBufStream& operator << (const manip::tab& x);
    GlyphBufStream& operator << (const manip::line_height& x);
    GlyphBufStream& operator << (const manip::cursor& x);
    GlyphBufStream& operator << (const manip::orig& x);

  private:
    void streamPair(wchar_t, wchar_t);
    void adjustRange(glyph_range const&, glm::vec2 const&);

    glm::vec2 cursor_;
    Font font_;
    glm::vec3 orig_;
    int tabSize_;
    int lineHeight_;
    uint32_t color_;
    std::vector<VUS3_CLIPUS1_CUB4_TUB3> attribs_;
    std::shared_ptr<FontArray> pArray_;
  };


} }

#endif
