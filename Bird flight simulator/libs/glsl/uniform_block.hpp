//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file uniform_block.hpp Support for \c ARB_uniform_buffer_object.

#ifndef GLSL_UNIFORM_BLOCK_HPP_INCLUDED
#define GLSL_UNIFORM_BLOCK_HPP_INCLUDED

#include "glsl.hpp"
#include "buffer.hpp"


namespace glsl {

  //! Thin Wrapper for GLSL Uniform Blocks
  class uniform_block
  {
  public:
    //! Constructs the uniform block from program-index pair
    uniform_block(GLuint program, GLuint index, GLint binding = 0);

    //! Constructs the uniform block from program-name pair
    uniform_block(GLuint program, const char* name, GLint binding = 0);

    GLuint program() const { return program_; }         //!< Returns associated program.
    GLuint index() const { return index_; }             //!< Returns the uniform block index.
    operator GLuint (void) const { return index_; }     //!< Cast to OpenGL block index.
    const char* name() const { return name_.c_str(); }  //!< Returns the name of the block.

    GLint binding(GLint binding);       //!< Sets the binding point
    GLint binding() const;              //!< Returns the binding point.
    GLint data_size() const;            //!< Returns the minimum data size.

  private:
    GLuint program_;
    GLuint index_;
    std::string name_;
  };


}  // namespace glsl


#endif  //glsl_uniform_block_hpp



