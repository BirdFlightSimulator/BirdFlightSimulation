/*
 * shader_pool.hpp
 *
 *  Created on: May 23, 2013
 *      Author: hanno
 */

#ifndef GLSL_SHADER_POOL_HPP_INCLUDED
#define GLSL_SHADER_POOL_HPP_INCLUDED

#include <memory>
#include <istream>
#include <unordered_map>
#include "program.hpp"
#include "buffer.hpp"


namespace glsl { 


  class shader_pool
  {
  public:
    typedef std::unordered_map<std::string, glsl::program> prog_map;
    typedef std::unordered_map<std::string, glsl::buffer> buffer_map;

  public:
    //! Parse the source in /c is
    void parse(std::istream& is);

    //! Returns the named program or nullptr. 
    glsl::program* operator()(const char* progName);

    //! Register named program
    bool add_named_program(const char* name, const glsl::program& prog);

    //! Retrieve named buffer or nullptr 
    glsl::buffer* get_named_buffer(const char* name);

    //! Register named buffer
    bool add_named_buffer(const char* name);

    prog_map::iterator begin() { return prog_map_.begin(); }    
    prog_map::iterator end() { return prog_map_.end(); }    
    prog_map::const_iterator begin() const { return prog_map_.begin(); }    
    prog_map::const_iterator end() const { return prog_map_.end(); }    
    prog_map::const_iterator cbegin() const { return prog_map_.cbegin(); }    
    prog_map::const_iterator cend() const { return prog_map_.cend(); }    
 
    //! Swap
    void swap(shader_pool& other);

  private:
    prog_map prog_map_;
    buffer_map buffer_map_;
  };

}


#endif