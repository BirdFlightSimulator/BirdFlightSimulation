//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file renderbuffer.hpp Thin Wrapper for OpenGL Renderbuffer Objects (rbo).
//! Supports:
//! \li \c ARB_framebuffer_object

#ifndef GLSL_HPP_RENDERBUFFER_INCLUDED
#define GLSL_HPP_RENDERBUFFER_INCLUDED

#include "glsl.hpp"
#include <cassert>
#include "proxy.hpp"


namespace glsl {

  //! Thin Wrapper for OpenGL %Renderbuffer Objects (rbo).
  class renderbuffer : proxy<renderbuffer>
  {
  public:
    //! Constructs 0-renderbuffer
    renderbuffer();

    //! Takes ownership over existing rbo
    explicit renderbuffer(GLuint rbo);

    //! Constructs a new rbo and allocates storage
    renderbuffer(GLenum internalFormat, GLsizei width, GLsizei height);

    ~renderbuffer();

    bool isValid() const { return isValid_(); }
    operator GLuint () const { return get_(); }    //!< Cast to underlying OpenGL object.
    GLuint get() const { return get_(); }      //!< Returns the underlying OpenGL object.

    //! Binds the rbo
    void bind() const;

    //! Unbinds the rbo
    void unbind() const;

    //! Allocates storage
    void storage(GLenum internalFormat, GLsizei width, GLsizei height) const;

    //! Allocates storage for multisampling
    void multisampleStorage(GLsizei samples, GLenum internalFormat, GLsizei width, GLsizei height) const;

    //! Swap
    void swap(renderbuffer& a);

  private:
    friend class proxy<renderbuffer>;
    static void release(GLuint x) { glDeleteRenderbuffers(1, &x); }
  };


}  // namespace glsl


#endif



