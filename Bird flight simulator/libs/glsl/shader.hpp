//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file shader.hpp Thin Wrapper for GLSL Fragment, Geometry and Vertext Shader.

#ifndef GLSL_SHADER_HPP_INCLUDED
#define GLSL_SHADER_HPP_INCLUDED

#include "glsl.hpp"
#include <string>
#include "proxy.hpp"


namespace glsl {


  //! Thin Wrapper for GLSL Fragment and Vertex %Shader.
  class shader : proxy<shader>
  {
  public:
    //! Constructs 0-shader
    shader();

    //! Constructs %shader from source and compiles
    //! \param shaderSource Source of the shader
    //! \param shaderType One of GL_GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, GL_GEOMETRY_SHADER
    shader(const char* shaderSource, GLenum shaderType);

    //! Frees resources
    ~shader(void);

    bool isValid() const { return isValid_(); }
    operator GLuint () const { return get_(); }    //!< Cast to underlying OpenGL object.
    GLuint get() const { return get_(); }      //!< Returns the underlying OpenGL object.

    GLint status(GLenum pname = GL_COMPILE_STATUS) const;  //!< Returns status
    GLenum type() const;                  //!< Returns GLSL %shader type
    std::string log_info() const;              //!< Returns log. info
    std::string source() const;                //!< Returns source

    void swap(shader& s);  //!< Swap

  private:
    friend class proxy<shader>;
    static void release(GLuint x) { glDeleteShader(x); }
  };


}  // namespace glsl


#endif
